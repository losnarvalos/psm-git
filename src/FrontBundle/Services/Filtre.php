<?php

namespace FrontBundle\Services;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;

class Filtre
{

    protected $em;

    protected $types = array("Roue", "Equerre", "Planche", "Divers", "Vis");

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function getEntitiesByType($type)
    {
        $produits = "";

        switch ($type) {
            case "Equerre" :
                $produits = $this->em->getRepository('BackBundle:ProduitGenerique')->getEquerre();
                break;
            case "Roue" :
                $produits = $this->em->getRepository('BackBundle:ProduitGenerique')->getRoue();
                break;
            case "Planche" :
                $produits = $this->em->getRepository('BackBundle:ProduitGenerique')->getPlanche();
                break;
            case "Vis" :
                $produits = $this->em->getRepository('BackBundle:ProduitGenerique')->getVis();
                break;
            case "Divers" :
                $produits = $this->em->getRepository('BackBundle:ProduitGenerique')->getDivers();
                break;
        }
        return $produits;
    }

    public function getEntitiesByColor($couleur)
    {
        $produitsGeneriques = $this->em->getRepository('BackBundle:ProduitGenerique')->findAll();
        $returnProduits = [];

        foreach ($produitsGeneriques as $produitGenerique) {
            if($produitGenerique->getProduit()->getCouleur() == $couleur) {
                $returnProduits[] = $produitGenerique;
            }
        }

        return $returnProduits;
    }
    
    public function getEntitiesBySearch($search)
    {
        $produits = new ArrayCollection();

        foreach($this->types as $type) {
            $produitsTypes = $this->em->getRepository('BackBundle:'.$type)->search($search);
            foreach($produitsTypes as $produitType) {
                $produits->add($produitType->getProduitGenerique());
            }
        }

       return $produits;
    }

}