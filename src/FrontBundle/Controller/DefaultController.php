<?php

namespace FrontBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use BackBundle\Entity\Couleur;
use BackBundle\Entity\Vis;
use BackBundle\Entity\Roue;
use BackBundle\Entity\Planche;
use BackBundle\Entity\Divers;
use BackBundle\Entity\Equerre;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->render('@Front/Default/index.html.twig');
    }

    /**
     * @Route("/catalogue", name="catalogue")
     * @Method("GET")
     */
    public function catalogueAction()
    {
        $em = $this->getDoctrine()->getManager();
        $produits = $em->getRepository('BackBundle:ProduitGenerique')->findAll();
        $couleurs = $this->get('configTools')->getAllCouleurs();
        $type = "";
        return $this->render('@Front/Default/catalogue.html.twig', array(
            'produits' => $produits,
            'couleursType' => $couleurs,
            'type' => $type,
        ));
    }


    /**
     * @Route("/{id}/{type}", name="fo_element_show", requirements={"id": "\d+"})
     */
    public function showAction($id, $type)
    {
        $em = $this->getDoctrine()->getManager();
        $entite = $em->getRepository("BackBundle:" . $type)->find($id);

        //recuperation du service
        $couleur = $this->get('configTools')->getCouleurType($type);

        return $this->render('FrontBundle:Default:show.html.twig', array(
            'produit' => $entite,
            'couleur' => $couleur
        ));
    }

    public function asideAction($type = null, $couleur = null)
    {
        $em = $this->getDoctrine()->getManager();
        $couleurs = $em->getRepository('BackBundle:Couleur')->findAll();
        return $this->render('FrontBundle:layout:aside.html.twig', array(
            'couleurs' => $couleurs,
            'type' => $type,
            'color' => $couleur,
        ));
    }


    /**
     * @Route("/recherche", name="recherche")
     * @Method({"GET","POST"})
     */
    public function searchAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('search', 'text', array('label' => 'Par Référence ou produit'))
            ->setAction($this->generateUrl('recherche'))
            ->setMethod('post')
            ->getForm();

        $form->handleRequest($request);
        $couleurs = $this->get('configTools')->getAllCouleurs();

        if ($request->isMethod('post')) {
            $produits = $this->get('filtremanager')->getEntitiesBySearch($form->getData()['search']);
            return $this->render('@Front/Default/catalogue.html.twig', array(
                'produits' => $produits,
                'couleursType' => $couleurs,
            ));
        }

        return $this->render('FrontBundle:layout:search.html.twig', array(
            'form' => $form->createView(),
        ));
    }


    /**
     * Filtre par type
     *
     * @Route("/filtre/{type}", name="filtre_type")
     * @Method("GET")
     */
    public function filtreTypeAction($type)
    {
        $produits = $this->get('filtreManager')->getEntitiesByType($type);
        $couleurs = $this->get('configTools')->getAllCouleurs();

        return $this->render('@Front/Default/catalogue.html.twig', array(
            'produits' => $produits,
            'couleursType' => $couleurs,
            'type' => $type,
        ));
    }


    /**
     * Filtre par couleur
     *
     * @Route("/filtre/couleur/{couleur}", name="filtre_couleur")
     * @Method("GET")
     */
    public function filtreCouleurAction(Couleur $couleur)
    {

        $produits = $this->get('filtreManager')->getEntitiesByColor($couleur);
        $couleurs = $this->get('configTools')->getAllCouleurs();

        return $this->render('@Front/Default/catalogue.html.twig', array(
            'produits' => $produits,
            'couleursType' => $couleurs,
            'couleur' => $couleur,
        ));
    }

    /**
     * Création d'un user lor d'un commit
     *
     * @Route("/populate", name="populate")
     * @Method("GET")
     */
    public function populateAction()
    {
        $um = $this->get('fos_user.user_manager')->findUsers();

        if (count($um) > 0) {
            return $this->redirectToRoute("fos_user_security_login");
            die();
        }

        return new Response($this->get('fos_user.util.user_manipulator')->create('admin', 'admin', 'admin@admin.com', 1, 1));
    }
}
