<?php

namespace FrontBundle\Controller;

use BackBundle\Entity\Equerre;
use BackBundle\Entity\ProduitGenerique;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use BackBundle\Entity\Panier;
use BackBundle\Entity\LignePanier;
use UserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


class PanierController extends Controller
{
    /**
     * @Route("/addProduct/{id}", name="addProduct")
     * @Method({"GET","POST"})
     */
    public function addProductAction(Request $request, ProduitGenerique $produitGenerique   )
    {
       $qte = $request->get('qte');

        $this->addLigne($produitGenerique, $qte);

        $em = $this->getDoctrine()->getManager();
        $produits = $em->getRepository('BackBundle:ProduitGenerique')->findAll();
        $couleurs = $this->get('configTools')->getAllCouleurs();
        $type = "";
        return $this->render('@Front/Default/catalogue.html.twig', array(
            'produits' => $produits,
            'couleursType' => $couleurs,
            'type' => $type,
            'addProduct' => $produitGenerique,
            'qte' => $qte
        ));

    }

    public function addLigne($produitGenerique, $qte){

        $em = $this->getDoctrine()->getManager();
        $panier = $this->getUser()->getPanier($em);
        $produits=[];
        foreach ($panier->getLignePanier() as $ligne) {
            $produits[$ligne->getId()] = $ligne->getProduitGenerique();
        }
        $key = array_search($produitGenerique, $produits);
        if($key != false) {
            $ligne = $panier->getLignePanierById($key);
            $ligne ->setQte($ligne->getQte() + $qte);
            $ajoutLigne = false;
        }else {
            $ajoutLigne = true;
        }

        if ($ajoutLigne) {
            $ligne = new LignePanier();
            $ligne->setQte($qte);
            $ligne->setPanier($panier);
            $ligne->setProduitGenerique($produitGenerique);
            $panier->addLignePanier($ligne);

            $em->persist($panier);
        }
        $em->persist($ligne);
        $em->flush();
    }

    /**
     * @Route("/clearPanier", name="clearPanier")
     */
    public function clearPanier()
    {
        $em = $this->getDoctrine()->getManager();
        $panier = $this->getUser()->getPanier($em);

        foreach ($panier->getLignePanier() as $ligne) {
            $em->remove($ligne);
        }

        $em->flush();

        return $this->redirectToRoute('panier');
    }

    public function iconAction()
    {
        $em = $this->getDoctrine()->getManager();
        $count = count($this->getUser()->getPanier($em)->getLignePanier());

        return $this->render('FrontBundle:layout:cart.html.twig', array(
            'count' =>$count,
        ));
    }
    
    
    public function displayFormCartAction(ProduitGenerique $produitGenerique)
    {
        return $this->render('FrontBundle:panier:addCart.html.twig', array(
            'produitGenerique' => $produitGenerique,
        ));
    }

    /**
     * @Route("/Panier", name="panier")
     */
    public function showAction () {

        $em = $this->getDoctrine()->getManager();
        $panier = $this->getUser()->getPanier($em);

        return $this->render('FrontBundle:panier:show.html.twig', array(
            'panier' => $panier,
        ));

    }

    /**
     * @Route("/changeQuantity/{id}", name="changeQuantity")
     * @Method({"GET","POST"})
     */
    public function changeQuantity(Request $request, LignePanier $lignePanier) {

        $qte = $request->get('qte');
        $em = $this->getDoctrine()->getManager();

        if($qte >=0) {  
            if($qte == 0) {
                $em->remove($lignePanier);
            }else {
                $lignePanier->setQte($qte);
                $em->persist($lignePanier);
            }
            $em->flush();
        }

        return $this->redirectToRoute('panier');
    }

    /**
     * @Route("/Ligne/delete/{id}", name="deleteLigne")
     * @Method({"GET","POST"})
     */
    public function deleteLineAction(Request $request, LignePanier $lignePanier) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($lignePanier);
        $em->flush();
        return $this->redirectToRoute('panier');
    }

}
