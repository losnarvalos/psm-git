<?php

namespace FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use UserBundle\Entity\User;
use BackBundle\Entity\Command;
use BackBundle\Entity\LigneCommand;

class CommandController extends Controller
{
    /**
     * @Route("/setCommand/{user}", name="setCommand")
     */
    public function setCommandAction(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $command = new Command();
        $command->setUser($user);
        $command->setState(0);
        $command->setType(0);
        foreach ($user->getPanier($em)->getLignePanier() as $lignePanier){
            $ligneCommand = new LigneCommand();
            $ligneCommand->setCommand($command);
            $ligneCommand->setProduitGenerique($lignePanier->getProduitGenerique());
            $ligneCommand->setQte($lignePanier->getQte());
            $command->addLignesCommand($ligneCommand);
            $em->persist($ligneCommand);
        }
        $em->persist($command);
        $em->remove($user->getPanier($em));
        $em->flush();

        $this->get('stockmanager')->setStock($command);

        $this->addFlash('success','Commande correctement ajouté');
        return $this->redirectToRoute('catalogue');
    }

}
