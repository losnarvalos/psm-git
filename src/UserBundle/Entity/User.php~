<?php

namespace UserBundle\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\User as BaseUser;
use BackBundle\Entity\Panier;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * One Customer has One Cart.
     * @ORM\OneToOne(targetEntity="BackBundle\Entity\Panier", mappedBy="user")
     */
    private $panier;

    /**
     * One Customer has Many Command.
     * @ORM\OneToOne(targetEntity="BackBundle\Entity\Command", mappedBy="user")
     */
    private $command;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Set panier
     *
     * @param \BackBundle\Entity\Panier $panier
     * @return User
     */
    public function setPanier(\BackBundle\Entity\Panier $panier = null)
    {
        $this->panier = $panier;

        return $this;
    }

    /**
     * Get panier
     *
     * @return \BackBundle\Entity\Panier 
     */
    public function getPanier(EntityManager $em)
    {
        if($this->panier == null) {
            $panier = new Panier();
            $panier->setUser($this);
            $em->persist($panier);
            $this->setPanier($panier);
            $em->persist($this);
            $em->flush();
        }

        return $this->panier;

    }
}