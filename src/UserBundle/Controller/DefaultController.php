<?php

namespace UserBundle\Controller;

use FOS\UserBundle\FOSUserBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use UserBundle\Entity\User;
use Symfony\Component\Routing\Annotation\Route;
use FOS\UserBundle\Form\Type\RegistrationFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="fos_user_list")
     */
    public function indexAction()
    {
        $usersManager = $this->get('fos_user.user_manager');
        $users = $usersManager->findUsers();

        return $this->render('UserBundle:Profile:list.html.twig', array(
            'users' => $users
        ));
    }

    /**
     * @Route("/delete/{id}", name="fos_user_delete")
     */
    public function deleteAction(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $panier = $user->getPanier($em);
        $lignePaniers = $panier->getLignePanier();
        $em->remove($panier);
        foreach ($lignePaniers as $lignePanier) {
            $em->remove($lignePanier);
        }
        $em->flush();
        $userManager = $this->get('fos_user.user_manager');
        $currentUser = $this->container->get('security.context')->getToken()->getUser();
        if ($user == $currentUser) {
            $this->addFlash('danger', 'Impossible de supprimer l\'utilisateur courant');
        } else if ($user->hasRole('ROLE_SUPER_ADMIN')) {
            $this->addFlash('danger', 'Impossible de supprimer l\'administrateur');
        } else {
            $userManager->deleteUser($user);
            $this->addFlash('success', 'La modification à été correctement éffectué');
        }

        return $this->redirectToRoute('fos_user_list');
    }

    /**
     * @Route("/promote/{id}/{role}", name="fos_user_promote")
     */
    public function promoteAction(User $user, $role)
    {
        if (!is_numeric($role) || $role > 2) {
            $this->addFlash('warning', 'Role non disponible');
            return $this->redirectToRoute('fos_user_list');
        }

        $user->removeRole("ROLE_ADMIN");
        $user->removeRole("ROLE_SUPER_ADMIN");
        if ($role == '1') {
            $user->addRole("ROLE_ADMIN");
        }
        if ($role == '2') {
            $user->addRole("ROLE_SUPER_ADMIN");
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        $this->addFlash('success', 'La modification à été correctement éffectué');
        return $this->redirectToRoute('fos_user_list');
    }

    /**
     * @Route("/ajouter", name="fos_user_add")
     * @Method({"GET", "POST"})
     */
    public function addAction(Request $request)
    {
        $user = new User();
        $form = $this->createFormBuilder()->add("login")->add("email", "email")->add("password", "password")->add("confirmation", "password")->getForm();

        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->getData()['password'] == $form->getData()['confirmation']) {
                $userManager = $this->get('fos_user.user_manager');
                $exists = $userManager->findUserBy(array('email' => $form->getData()["email"]));
                $exists2 = $userManager->findUserBy(array('username' => $form->getData()["login"]));
                if ($exists instanceof User || $exists2 instanceof User) {
                    $this->addFlash('warning', 'Utilisateur déjà présent en base de données.');
                    return $this->redirectToRoute('fos_user_add');
                }
                $this->get('fos_user.util.user_manipulator')->create($form->getData()['login'], $form->getData()['password'], $form->getData()['email'], 1, 0);
                return $this->redirectToRoute('fos_user_list');
            } else {
                $this->addFlash('warning', 'Les mots de passe ne correspondent pas.');
                return $this->redirectToRoute('fos_user_add');
            }
        }
        return $this->render('UserBundle:Registration:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }


}
