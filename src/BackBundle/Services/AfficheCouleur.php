<?php
/**
 * Created by PhpStorm.
 * User: jeremynouveau
 * Date: 27/01/2017
 * Time: 14:06
 */

namespace BackBundle\Services;

use Doctrine\ORM\EntityManager;
use BackBundle\Entity\Vis;
use BackBundle\Entity\Planche;
use BackBundle\Entity\Roue;
use BackBundle\Entity\Equerre;
use BackBundle\Entity\Divers;
use BackBundle\Entity\Couleur;

class AfficheCouleur
{
    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function attrCouleur($class,$entity,Couleur $couleur){

        $obj = $this->em->getRepository('BackBundle:'.ucfirst($class))->find($entity);
        $obj->setCouleur($couleur);
        $this->em->persist($obj);
        $this->em->flush();

    }
    
}