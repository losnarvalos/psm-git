<?php
/**
 * Created by PhpStorm.
 * User: jeremynouveau
 * Date: 27/01/2017
 * Time: 14:06
 */

namespace BackBundle\Services;

use BackBundle\Entity\Command;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;


class PDFManager
{
    private $em;
    private $knpSnappy;
    private $twig;
    private $configTool;
    private $stockManager;

    public function __construct(EntityManager $entityManager, $twig,  $knpSnappy, $configTool, $stockManager)
    {
        $this->em = $entityManager;
        $this->twig = $twig;
        $this->knpSnappy = $knpSnappy;
        $this->configTool = $configTool;
        $this->stockManager = $stockManager;
    }

    public function getHeader(Command $command = null)
    {
       return $this->twig->render('BackBundle:PDF:header.html.twig', array(
           'command' => $command
       ));
    }

    public function getFooterTop()
    {
        return $this->twig->render('BackBundle:PDF:footer-top.html.twig');
    }

    public function getFooter(Command $command = null , $total = null)
    {
        return $this->twig->render('BackBundle:PDF:footer.html.twig', array(
            'command' => $command,
            'total' => $total
        ));
    }

    public function generateDedePDF(Command $command)
    {
        $date = new \DateTime();
        $fileName = 'PDF/dede-' . $date->format('dmYHis') . '.pdf';
        $this->knpSnappy->setOption('header-html' , $this->getHeader($command));
        $this->knpSnappy->setOption('footer-html' , $this->getFooter($command));
        $this->knpSnappy->generateFromHtml(
            $this->twig->render(
                'BackBundle:PDF:index.html.twig', array(
                    'command' => $command
                )
            ),
            $fileName
        );
        return $fileName;
    }

    public function generateProviderPDF()
    {
        $produits = $this->getAllProductsForCustomers();        
        $total = $this->getSumProductsForCustomers($produits);
        $date = new \DateTime();
        $fileName = 'PDF/provider-' . $date->format('dmYHis') . '.pdf';
        $this->knpSnappy->setOption('header-html' , $this->getHeader());
        $this->knpSnappy->setOption('footer-html' , $this->getFooter(null, $total));
        $this->knpSnappy->generateFromHtml(
            $this->twig->render(
                'BackBundle:PDF:provider.html.twig', array(
                    'produits' => $produits,
                )
            ),
            $fileName
        );
        return $fileName;
    }

    public function getAllProductsForCustomers()
    {
        $produitsMini = $this->stockManager->listeProduitStockMini();
        $produitsAlert = $this->stockManager->listeProduitStockAlert();
        $produits = new ArrayCollection(
            array_merge($produitsMini->toArray(), $produitsAlert->toArray())
        );
        return $produits;
    }

    public function getSumProductsForCustomers($produits)
    {
        $total = 0;
        foreach($produits as $produit)
        {
            foreach($produit as $item)
            {
                $total += $item->getNbCommandes() * $item->getPrix();
            }
        }
        return $total;
    }

}