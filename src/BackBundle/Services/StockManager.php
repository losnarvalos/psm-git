<?php
/**
 * Created by PhpStorm.
 * User: jeremynouveau
 * Date: 27/01/2017
 * Time: 14:06
 */

namespace BackBundle\Services;

use BackBundle\Entity\Command;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use BackBundle\Entity\ProduitGenerique;

class StockManager
{
    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function listeProduitStockAlert(){

        $vis = $this->em->getRepository("BackBundle:Vis")->findAll();

        $visStockAlerte = new ArrayCollection();
        foreach ($vis as $vi) {
            if($this->isStockAlert($vi->getStock(),$vi->getStockAlerte())){
                $visStockAlerte->add($vi);
            }
        }

        $planches = $this->em->getRepository("BackBundle:Planche")->findAll();

        $planchesStockAlerte = new ArrayCollection();
        foreach ($planches as $planche) {
            if($this->isStockAlert($planche->getStock(),$planche->getStockAlerte())){
                $planchesStockAlerte->add($planche);
            }
        }

        $equerres = $this->em->getRepository("BackBundle:Equerre")->findAll();

        $equerresStockAlerte = new ArrayCollection();
        foreach ($equerres as $equerre) {
            if($this->isStockAlert($equerre->getStock(),$equerre->getStockAlerte())){
                $equerresStockAlerte->add($equerre);
            }
        }

        $roues = $this->em->getRepository("BackBundle:Roue")->findAll();

        $rouesStockAlerte = new ArrayCollection();
        foreach ($roues as $roue) {
            if($this->isStockAlert($roue->getStock(),$roue->getStockAlerte())){
                $rouesStockAlerte->add($roue);
            }
        }

        $divers = $this->em->getRepository("BackBundle:Divers")->findAll();

        $diversStockAlerte = new ArrayCollection();
        foreach ($divers as $diver) {
            if($this->isStockAlert($diver->getStock(),$diver->getStockAlerte())){
                $diversStockAlerte->add($diver);
            }
        }

        $produitsStockAlert = new ArrayCollection();
        $produitsStockAlert ->add($visStockAlerte);
        $produitsStockAlert ->add($planchesStockAlerte);
        $produitsStockAlert ->add($rouesStockAlerte);
        $produitsStockAlert ->add($equerresStockAlerte);
        $produitsStockAlert ->add($diversStockAlerte);

        return $produitsStockAlert;
    }

    public function listeProduitStockMini()
    {
        $vis = $this->em->getRepository("BackBundle:Vis")->findAll();

        $visStockAlerte = new ArrayCollection();
        foreach ($vis as $vi) {
            if($this->isStockMini($vi->getStock(),$vi->getStockMini(),$vi->getStockAlerte())){
                $visStockAlerte->add($vi);
            }
        }

        $planches = $this->em->getRepository("BackBundle:Planche")->findAll();

        $planchesStockAlerte = new ArrayCollection();
        foreach ($planches as $planche) {
            if($this->isStockMini($planche->getStock(),$planche->getStockMini(),$planche->getStockAlerte())){
                $planchesStockAlerte->add($planche);
            }
        }

        $equerres = $this->em->getRepository("BackBundle:Equerre")->findAll();

        $equerresStockAlerte = new ArrayCollection();
        foreach ($equerres as $equerre) {
            if($this->isStockMini($equerre->getStock(),$equerre->getStockMini(),$equerre->getStockAlerte())){
                $equerresStockAlerte->add($equerre);
            }
        }

        $roues = $this->em->getRepository("BackBundle:Roue")->findAll();

        $rouesStockAlerte = new ArrayCollection();
        foreach ($roues as $roue) {
            if($this->isStockMini($roue->getStock(),$roue->getStockMini(),$roue->getStockAlerte())){
                $rouesStockAlerte->add($roue);
            }
        }

        $divers = $this->em->getRepository("BackBundle:Divers")->findAll();

        $diversStockAlerte = new ArrayCollection();
        foreach ($divers as $diver) {
            if($this->isStockMini($diver->getStock(),$diver->getStockMini(),$diver->getStockAlerte())){
                $diversStockAlerte->add($diver);
            }
        }

        $produitsStockMini = new ArrayCollection();
        $produitsStockMini ->add($visStockAlerte);
        $produitsStockMini ->add($planchesStockAlerte);
        $produitsStockMini ->add($rouesStockAlerte);
        $produitsStockMini ->add($equerresStockAlerte);
        $produitsStockMini ->add($diversStockAlerte);

        return $produitsStockMini;
    }


    public function listeProduitStockOk()
    {
        $vis = $this->em->getRepository("BackBundle:Vis")->findAll();

        $visStockAlerte = new ArrayCollection();
        foreach ($vis as $vi) {
            if($this->isStockOk($vi->getStock(),$vi->getStockMini())){
                $visStockAlerte->add($vi);
            }
        }

        $planches = $this->em->getRepository("BackBundle:Planche")->findAll();

        $planchesStockAlerte = new ArrayCollection();
        foreach ($planches as $planche) {
            if($this->isStockOk($planche->getStock(),$planche->getStockMini())){
                $planchesStockAlerte->add($planche);
            }
        }

        $equerres = $this->em->getRepository("BackBundle:Equerre")->findAll();

        $equerresStockAlerte = new ArrayCollection();
        foreach ($equerres as $equerre) {
            if($this->isStockOk($equerre->getStock(),$equerre->getStockMini())){
                $equerresStockAlerte->add($equerre);
            }
        }

        $roues = $this->em->getRepository("BackBundle:Roue")->findAll();

        $rouesStockAlerte = new ArrayCollection();
        foreach ($roues as $roue) {
            if($this->isStockOk($roue->getStock(),$roue->getStockMini())){
                $rouesStockAlerte->add($roue);
            }
        }

        $divers = $this->em->getRepository("BackBundle:Divers")->findAll();

        $diversStockAlerte = new ArrayCollection();
        foreach ($divers as $diver) {
            if($this->isStockOk($diver->getStock(),$diver->getStockMini())){
                $diversStockAlerte->add($diver);
            }
        }

        $produitsStockOk = new ArrayCollection();
        $produitsStockOk ->add($visStockAlerte);
        $produitsStockOk ->add($planchesStockAlerte);
        $produitsStockOk ->add($rouesStockAlerte);
        $produitsStockOk ->add($equerresStockAlerte);
        $produitsStockOk ->add($diversStockAlerte);

        return $produitsStockOk;
    }


    public function isStockAlert($stock,$stockAlert)
    {
        return $stockAlert >= $stock;
    }

    public function isStockMini($stock,$stockMini,$stockAlert)
    {
        return $stockAlert < $stock && $stock <= $stockMini ;
    }

    public function isStockOk($stock,$stockMini)
    {
        return $stock >= $stockMini;
    }

    public function nbOrder($produits)
    {
        $nb = 0;
        foreach ($produits as $listeProduit) {
            foreach ($listeProduit as $produit){
                $nb += $produit->getNbCommandes();
            }
        }
        return $nb ;
    }

    public function getCount($produits)
    {
        $tab = array();
        for($i = 0; $i < count($produits); $i++){
            $tab[] = count($produits[$i]);
        }
        return $tab;
    }

    public function setStock(Command $command)
    {
        foreach ($command->getLignesCommand() as $ligne)
        {
            $produit = $ligne->getProduitGenerique()->getProduit();
            $stock = $produit->getStock();
            $qte = $ligne->getQte();
            $valeur = $stock-$qte;
            $produit->setStock($valeur);
            $this->em->persist($produit);
        }
        $this->em->flush($produit);
    }

    public function resetStock(Command $command)
    {
        foreach ($command->getLignesCommand() as $ligne)
        {
            $produit = $ligne->getProduitGenerique()->getProduit();
            
            $stock = $produit->getStock();
            
            $nbCommandes = $produit->getNbCommandes();
            $valeur = $stock+$nbCommandes;
            $produit->setStock($valeur);
            $this->em->persist($produit);
        }
        $this->em->flush($produit);
    }

    public function isInCurrentOrder(ProduitGenerique $produitGenerique)
    {
        $return = false;
        $commandes = $this->em->getRepository("BackBundle:Command")->findBy(array("type"=>1,"state"=>0));
        $array = new ArrayCollection($commandes);
        foreach ($array as $row)
        {
            foreach ($row->getLignesCommand() as $ligne){
                if ($ligne->getProduitGenerique() == $produitGenerique)
                {
                    $return = true;
                }
            }
        }
        return $return;
    }

}