<?php
/**
 * Created by PhpStorm.
 * User: jeremynouveau
 * Date: 27/01/2017
 * Time: 14:06
 */

namespace BackBundle\Services;

use Doctrine\ORM\EntityManager;

class ConfigTools
{
    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function getCouleurType($type){
        switch ($type){
            case 'Planche' :
                $return = $this-> em -> getRepository("BackBundle:Configuration")->findOneBy(['nom'=>'couleur_planche'])->getValeur();
                break;
            case 'Vis' :
                $return = $this-> em -> getRepository("BackBundle:Configuration")->findOneBy(['nom'=>'couleur_vis'])->getValeur();
                break;
            case 'Roue' :
                $return = $this-> em -> getRepository("BackBundle:Configuration")->findOneBy(['nom'=>'couleur_roue'])->getValeur();
                break;
            case 'Equerre' :
                $return = $this-> em -> getRepository("BackBundle:Configuration")->findOneBy(['nom'=>'couleur_equerre'])->getValeur();
                break;
            case 'Divers' :
                $return = $this-> em -> getRepository("BackBundle:Configuration")->findOneBy(['nom'=>'couleur_divers'])->getValeur();
                break;
            default:
                $return = '#ffffff';
        }

        return $return;
    }


    public function getConfig($key)
    {
        return $this-> em -> getRepository("BackBundle:Configuration")->findOneBy(['nom'=>$key])->getValeur();
    }

    public function setConfig($key, $value)
    {
        $field = $this-> em -> getRepository("BackBundle:Configuration")->findOneBy(['nom'=>$key]);
        $field->setValeur($value);
        $this->em->persist($field);
        $this->em->flush();
    }

    public function generateFormSociete($fb)
    {
        return $fb
            ->add('societe', 'text' , array(
                'attr' => array(
                    'value' => $this->getConfig('nomSociete')
                )
            ))
            ->add('adresse', 'text' , array(
                'attr' => array(
                    'value' => $this->getConfig('adresseSociete')
                )
            ))
            ->add('complement', 'text' , array(
                'attr' => array(
                    'value' => $this->getConfig('complementAdresseSociete')
                )
            ))
            ->add('cp', 'number' , array(
                'attr' => array(
                    'value' => $this->getConfig('cpSociete')
                )
            ))
            ->add('ville', 'text' , array(
                'attr' => array(
                    'value' => $this->getConfig('villeSociete')
                )
            ))
            ->add('siret', 'text' , array(
                'attr' => array(
                    'value' => $this->getConfig('SIRETSociete')
                )
            ))
            ->getForm();
    }

    public function bindPostSociete($form)
    {
        $this->setConfig('nomSociete', $form->getData()['societe']);
        $this->setConfig('adresseSociete', $form->getData()['adresse']);
        $this->setConfig('complementAdresseSociete', $form->getData()['complement']);
        $this->setConfig('cpSociete', $form->getData()['cp']);
        $this->setConfig('villeSociete', $form->getData()['ville']);
        $this->setConfig('SIRETSociete', $form->getData()['siret']);
    }

    public function generateFormFournisseur($fb)
    {
        return $fb
            ->add('fournisseur', 'text' , array(
                'attr' => array(
                    'value' => $this->getConfig('nomFournisseur')
                )
            ))
            ->add('adresse', 'text' , array(
                'attr' => array(
                    'value' => $this->getConfig('adresseFournisseur')
                )
            ))
            ->add('complement', 'text' , array(
                'attr' => array(
                    'value' => $this->getConfig('complementAdresseFournisseur')
                )
            ))
            ->add('cp', 'number' , array(
                'attr' => array(
                    'value' => $this->getConfig('cpFournisseur')
                )
            ))
            ->add('ville', 'text' , array(
                'attr' => array(
                    'value' => $this->getConfig('villeFournisseur')
                )
            ))
            ->add('siret', 'text' , array(
                'attr' => array(
                    'value' => $this->getConfig('SIRETFournisseur')
                )
            ))
            ->getForm();
    }

    public function bindPostFournisseur($form)
    {
        $this->setConfig('nomFournisseur', $form->getData()['fournisseur']);
        $this->setConfig('adresseFournisseur', $form->getData()['adresse']);
        $this->setConfig('complementAdresseFournisseur', $form->getData()['complement']);
        $this->setConfig('cpFournisseur', $form->getData()['cp']);
        $this->setConfig('villeFournisseur', $form->getData()['ville']);
        $this->setConfig('SIRETFournisseur', $form->getData()['siret']);
    }

    public function getAllCouleurs(){
        $couleurs["Roue"] =$this-> em -> getRepository("BackBundle:Configuration")->findOneBy(['nom'=>'couleur_roue'])->getValeur();
        $couleurs["Vis"] =$this-> em -> getRepository("BackBundle:Configuration")->findOneBy(['nom'=>'couleur_vis'])->getValeur();
        $couleurs["Planche"] =$this-> em -> getRepository("BackBundle:Configuration")->findOneBy(['nom'=>'couleur_planche'])->getValeur();
        $couleurs["Equerre"] =$this-> em -> getRepository("BackBundle:Configuration")->findOneBy(['nom'=>'couleur_equerre'])->getValeur();
        $couleurs["Divers"] =$this-> em -> getRepository("BackBundle:Configuration")->findOneBy(['nom'=>'couleur_divers'])->getValeur();
        return $couleurs;
    }

    public function getEntityByGenerique($id){

        return $this-> em -> getRepository("BackBundle:ProduitGenerique")->findOneBy(['id'=>$id])->getProduit();

    }

}