<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EquerreType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom')
            ->add('reference')
            ->add('longueur_min')
            ->add('longueur_max')
            ->add('angle')
            ->add('poids')
            ->add('emplacement')
            ->add('prix')
            ->add('description')
            ->add('nb_trous')
            ->add('stock_mini')
            ->add('stock_alerte')
            ->add('stock')
            ->add('nb_commandes')
            ->add('date_reapro_dernier', null,array('widget' => 'single_text',
                'input' => 'datetime','attr' => array('class' => 'date-pick')))
            ->add('date_reapro_prochain', null,array('widget' => 'single_text',
                'input' => 'datetime','attr' => array('class' => 'date-pick')))
            ->add('materiau');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Equerre'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_equerre';
    }


}
