<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VisType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom')->add('reference')->add('poids')->add('longueur')->add('diametre_tete')->add('diametre_filetage')->add('emplacement')->add('prix')->add('description')->add('taille')->add('nb_commandes')->add('date_reapro_dernier')->add('date_reapro_prochain')->add('stock_mini')->add('stock_alerte')->add('stock')->add('typetete')->add('typefiletage')->add('typevissage');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Vis'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_vis';
    }


}
