<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlancheType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom')->add('reference')->add('longueur')->add('largeur')->add('epaisseur')
            ->add('emplacement')->add('prix')->add('description')->add('humidite')->add('durete')
            ->add('taille')->add('stock_mini')->add('stock_alerte')->add('stock')
            ->add('nb_commandes')->add('date_reapro_dernier', null,array('widget' => 'single_text',
                'input' => 'datetime','attr' => array('class' => 'date-pick')))->add('date_reapro_prochain', null,array('widget' => 'single_text',
        'input' => 'datetime','attr' => array('class' => 'date-pick')))
        ->add('poids')->add('essence');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Planche'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_planche';
    }


}
