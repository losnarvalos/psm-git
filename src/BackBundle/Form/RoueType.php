<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoueType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom')->add('reference')->add('emplacement')->add('diametre')->add('nbCommandes')->add('prix')->add('poids')->add('description')->add('hauteur_pneu')->add('nb_boulons')->add('stock_mini')->add('stock_alerte')->add('stock')->add('date_reapro_dernier', null,array('widget' => 'single_text',
            'input' => 'datetime','attr' => array('class' => 'date-pick')))->add('date_reapro_prochain', null,array('widget' => 'single_text',
            'input' => 'datetime','attr' => array('class' => 'date-pick')));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Roue'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_roue';
    }


}
