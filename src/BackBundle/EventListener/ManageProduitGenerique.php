<?php
namespace BackBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use BackBundle\Entity\ProduitGenerique;
use BackBundle\Entity\Roue;
use BackBundle\Entity\Divers;
use BackBundle\Entity\Equerre;
use BackBundle\Entity\Planche;
use BackBundle\Entity\Vis;

class ManageProduitGenerique
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Roue) {
            $pG = new ProduitGenerique();
            $pG->setRoue($entity);
            $entity->setProduitGenerique($pG);
            $em->persist($pG);
        }else if($entity instanceof Divers) {
            $pG = new ProduitGenerique();
            $pG->setDivers($entity);
            $entity->setProduitGenerique($pG);
            $em->persist($pG);
        }else if($entity instanceof Equerre) {
            $pG = new ProduitGenerique();
            $pG->setEquerre($entity);
            $entity->setProduitGenerique($pG);
            $em->persist($pG);
        }else if($entity instanceof Planche) {
            $pG = new ProduitGenerique();
            $pG->setPlanche($entity);
            $entity->setProduitGenerique($pG);
            $em->persist($pG);
        }else if($entity instanceof Vis) {
            $pG = new ProduitGenerique();
            $pG->setVis($entity);
            $entity->setProduitGenerique($pG);
            $em->persist($pG);
        }
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Roue || $entity instanceof Divers || $entity instanceof Equerre || $entity instanceof Planche || $entity instanceof Planche || $entity instanceof Vis) {
            $pg = $entity->getProduitGenerique();
            $em->remove($pg);
        }
    }
}

