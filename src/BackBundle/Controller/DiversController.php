<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Divers;
use BackBundle\Entity\Photo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Diver controller.
 *
 * @Route("divers")
 */
class DiversController extends Controller
{
    /**
     * Lists all diver entities.
     *
     * @Route("/", name="divers_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $divers = $em->getRepository('BackBundle:Divers')->findAll();

        return $this->render('BackBundle:divers:index.html.twig', array(
            'divers' => $divers,
        ));
    }

    /**
     * Creates a new diver entity.
     *
     * @Route("/new", name="divers_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $diver = new Divers();
        $form = $this->createForm('BackBundle\Form\DiversType', $diver);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($diver);
            $em->flush($diver);

            return $this->redirectToRoute('divers_show', array('id' => $diver->getId()));
        }

        return $this->render('BackBundle:divers:new.html.twig', array(
            'diver' => $diver,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a diver entity.
     *
     * @Route("/{id}", name="divers_show")
     * @Method("GET")
     */
    public function showAction(Divers $diver)
    {
        $deleteForm = $this->createDeleteForm($diver);

        return $this->render('BackBundle:divers:show.html.twig', array(
            'diver' => $diver,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing diver entity.
     *
     * @Route("/{id}/edit", name="divers_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Divers $diver)
    {
        $deleteForm = $this->createDeleteForm($diver);
        $editForm = $this->createForm('BackBundle\Form\DiversType', $diver);
        $editForm->handleRequest($request);

        /* images */
        $em = $this->getDoctrine()->getManager();
        $images = $em->getRepository('BackBundle:Photo')->findAll();
        $couleurs = $em->getRepository('BackBundle:Couleur')->findAll();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('divers_edit', array('id' => $diver->getId()));
        }

        return $this->render('BackBundle:divers:editCustom.html.twig', array(
            'divers' => $diver,
            'couleurs' => $couleurs,
            'images' => $images,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a diver entity.
     *
     * @Route("/suppression/{id}", name="divers_delete")
     * @Method({"GET", "DELETE"})
     */
    public function deleteAction(Request $request, Divers $diver)
    {
    $em = $this->getDoctrine()->getManager();
    $em->remove($diver);
    $em->flush();

        return $this->redirectToRoute('divers_index');
    }

    /**
     * Creates a form to delete a diver entity.
     *
     * @param Divers $diver The diver entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Divers $diver)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('divers_delete', array('id' => $diver->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Add Image
     * @Route("/addImage/{idDivers}/{idPhoto}/{isMain}", name="divers_addImage")
     * @ParamConverter("divers", options={"mapping": {"idDivers": "id"}})
     * @ParamConverter("photo", options={"mapping": {"idPhoto": "id"}})
     */
    public function addImage(Divers $divers,Photo $photo, $isMain)
    {
        $em = $this->getDoctrine()->getManager();
        $photo->setDivers($divers);
        $photo->setEstPrincipal($isMain);
        $em->persist($photo);
        $em->flush();
        return $this->redirectToRoute("divers_edit", array("id"=>$divers->getId()));
    }

    /**
     * Add Image
     * @Route("/addImage/{idDivers}/{idPhoto}", name="divers_removeImage")
     * @ParamConverter("divers", options={"mapping": {"idDivers": "id"}})
     * @ParamConverter("photo", options={"mapping": {"idPhoto": "id"}})
     */
    public function removeImage(Divers $divers,Photo $photo)
    {
        $em = $this->getDoctrine()->getManager();
        $photo->setDivers(null);
        $em->persist($photo);
        $em->flush();
        return $this->redirectToRoute("divers_edit", array("id"=>$divers->getId()));
    }


}
