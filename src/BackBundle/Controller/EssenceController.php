<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Essence;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Essence controller.
 *
 * @Route("essence")
 */
class EssenceController extends Controller
{
    /**
     * Lists all essence entities.
     *
     * @Route("/", name="essence_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $essences = $em->getRepository('BackBundle:Essence')->findAll();

        return $this->render('BackBundle:essence:index.html.twig', array(
            'essences' => $essences,
        ));
    }

    /**
     * Creates a new essence entity.
     *
     * @Route("/new", name="essence_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $essence = new Essence();
        $form = $this->createForm('BackBundle\Form\EssenceType', $essence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($essence);
            $em->flush($essence);

            return $this->redirectToRoute('essence_show', array('id' => $essence->getId()));
        }

        return $this->render('BackBundle:essence:new.html.twig', array(
            'essence' => $essence,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a essence entity.
     *
     * @Route("/{id}", name="essence_show")
     * @Method("GET")
     */
    public function showAction(Essence $essence)
    {
        $deleteForm = $this->createDeleteForm($essence);

        return $this->render('BackBundle:essence:show.html.twig', array(
            'essence' => $essence,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing essence entity.
     *
     * @Route("/{id}/edit", name="essence_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Essence $essence)
    {
        $deleteForm = $this->createDeleteForm($essence);
        $editForm = $this->createForm('BackBundle\Form\EssenceType', $essence);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('essence_edit', array('id' => $essence->getId()));
        }

        return $this->render('BackBundle:essence:edit.html.twig', array(
            'essence' => $essence,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a essence entity.
     *
     * @Route("/{id}", name="essence_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Essence $essence)
    {
        $form = $this->createDeleteForm($essence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($essence);
            $em->flush($essence);
        }

        return $this->redirectToRoute('essence_index');
    }

    /**
     * Creates a form to delete a essence entity.
     *
     * @param Essence $essence The essence entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Essence $essence)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('essence_delete', array('id' => $essence->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
