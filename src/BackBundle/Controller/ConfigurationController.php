<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Configuration;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Configuration controller.
 *
 * @Route("configuration")
 */
class ConfigurationController extends Controller
{
    /**
     * Lists all configuration entities.
     *
     * @Route("/", name="configuration_index")
     * @Method("GET")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $configurations = $em->getRepository('BackBundle:Configuration')->findAll();

        return $this->render('BackBundle:configuration:index.html.twig', array(
            'configurations' => $configurations,
        ));
    }

    /**
     * Creates a new configuration entity.
     *
     * @Route("/new", name="configuration_new")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function newAction(Request $request)
    {
        $configuration = new Configuration();
        $form = $this->createForm('BackBundle\Form\ConfigurationType', $configuration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($configuration);
            $em->flush($configuration);

            return $this->redirectToRoute('configuration_show', array('id' => $configuration->getId()));
        }

        return $this->render('BackBundle:configuration:new.html.twig', array(
            'configuration' => $configuration,
            'form' => $form->createView(),
        ));
    }

    /**
    * Creates a new configuration entity.
    *
    * @Route("/editSociete", name="edit_societe")
    * @Method({"GET", "POST"})
    * @Security("has_role('ROLE_SUPER_ADMIN')")
    */
    public function editSocieteAction(Request $request)
    {
        $config = $this->get('configtools');
        $form = $config->generateFormSociete($this->createFormBuilder());
        $form->handleRequest($request);
        if ($request->isMethod('POST')) {
            $config->bindPostSociete($form);
            $this->addFlash('success' , 'Les modifications ont bien été effectuées!');
            return $this->redirectToRoute('edit_societe');
        }

        return $this->render('BackBundle:configurationSociete:edit.html.twig', array(
            'edit_form' => $form->createView(),
        ));
    }

    /**
    * Creates a new configuration entity.
    *
    * @Route("/editFournisseur", name="edit_fournisseur")
    * @Method({"GET", "POST"})
    * @Security("has_role('ROLE_SUPER_ADMIN')")
    */
    public function editFournisseurAction(Request $request)
    {
        $config = $this->get('configtools');
        $form = $config->generateFormFournisseur($this->createFormBuilder());
        $form->handleRequest($request);
        if ($request->isMethod('POST')) {
            $config->bindPostFournisseur($form);
            $this->addFlash('success' , 'Les modifications ont bien été effectuées!');
            return $this->redirectToRoute('edit_fournisseur');
        }

        return $this->render('BackBundle:configurationFournisseur:edit.html.twig', array(
            'edit_form' => $form->createView(),
        ));
    }


    /**
     * Creates a new configuration entity.
     *
     * @Route("/newColor", name="configuration_new_color")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function newColorAction(Request $request)
    {
        $configuration = new Configuration();
        $form = $this->createForm('BackBundle\Form\ConfigurationColorType', $configuration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($configuration);
            $em->flush($configuration);

            return $this->redirectToRoute('configuration_show', array('id' => $configuration->getId()));
        }

        return $this->render('BackBundle:configuration:new.html.twig', array(
            'configuration' => $configuration,
            'form' => $form->createView(),
        ));
    }


    /**
     * Finds and displays a configuration entity.
     *
     * @Route("/{id}", name="configuration_show")
     * @Method("GET")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function showAction(Configuration $configuration)
    {
        $deleteForm = $this->createDeleteForm($configuration);

        return $this->render('BackBundle:configuration:show.html.twig', array(
            'configuration' => $configuration,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing configuration entity.
     *
     * @Route("/{id}/edit", name="configuration_edit")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function editAction(Request $request, Configuration $configuration)
    {
        $deleteForm = $this->createDeleteForm($configuration);
        $editForm = $this->createForm('BackBundle\Form\ConfigurationType', $configuration);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('configuration_edit', array('id' => $configuration->getId()));
        }

        return $this->render('BackBundle:configuration:edit.html.twig', array(
            'configuration' => $configuration,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a configuration entity.
     *
     * @Route("/{id}", name="configuration_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function deleteAction(Request $request, Configuration $configuration)
    {
        $form = $this->createDeleteForm($configuration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($configuration);
            $em->flush($configuration);
        }

        return $this->redirectToRoute('configuration_index');
    }

    /**
     * Creates a form to delete a configuration entity.
     *
     * @param Configuration $configuration The configuration entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Configuration $configuration)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('configuration_delete', array('id' => $configuration->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
