<?php

namespace BackBundle\Controller;

use BackBundle\Entity\TypeTete;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Typetete controller.
 *
 * @Route("typetete")
 */
class TypeTeteController extends Controller
{
    /**
     * Lists all typeTete entities.
     *
     * @Route("/", name="typetete_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $typeTetes = $em->getRepository('BackBundle:TypeTete')->findAll();

        return $this->render('BackBundle:typetete:index.html.twig', array(
            'typeTetes' => $typeTetes,
        ));
    }

    /**
     * Creates a new typeTete entity.
     *
     * @Route("/new", name="typetete_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $typeTete = new Typetete();
        $form = $this->createForm('BackBundle\Form\TypeTeteType', $typeTete);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typeTete);
            $em->flush($typeTete);

            return $this->redirectToRoute('typetete_show', array('id' => $typeTete->getId()));
        }

        return $this->render('BackBundle:typetete:new.html.twig', array(
            'typeTete' => $typeTete,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a typeTete entity.
     *
     * @Route("/{id}", name="typetete_show")
     * @Method("GET")
     */
    public function showAction(TypeTete $typeTete)
    {
        $deleteForm = $this->createDeleteForm($typeTete);

        return $this->render('BackBundle:typetete:show.html.twig', array(
            'typeTete' => $typeTete,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing typeTete entity.
     *
     * @Route("/{id}/edit", name="typetete_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TypeTete $typeTete)
    {
        $deleteForm = $this->createDeleteForm($typeTete);
        $editForm = $this->createForm('BackBundle\Form\TypeTeteType', $typeTete);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('typetete_edit', array('id' => $typeTete->getId()));
        }

        return $this->render('BackBundle:typetete:edit.html.twig', array(
            'typeTete' => $typeTete,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a typeTete entity.
     *
     * @Route("/{id}", name="typetete_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TypeTete $typeTete)
    {
        $form = $this->createDeleteForm($typeTete);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typeTete);
            $em->flush($typeTete);
        }

        return $this->redirectToRoute('typetete_index');
    }

    /**
     * Creates a form to delete a typeTete entity.
     *
     * @param TypeTete $typeTete The typeTete entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TypeTete $typeTete)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('typetete_delete', array('id' => $typeTete->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
