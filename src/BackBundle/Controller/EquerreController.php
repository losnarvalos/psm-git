<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Equerre;
use BackBundle\Entity\Photo;
use BackBundle\Entity\Couleur;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use BackBundle\Services\AfficheCouleur;
use Symfony\Component\HttpFoundation\Response;

/**
 * Equerre controller.
 *
 * @Route("equerre")
 */
class EquerreController extends Controller
{
    /**
     * Lists all equerre entities.
     *
     * @Route("/", name="equerre_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $equerres = $em->getRepository('BackBundle:Equerre')->findAll();

        return $this->render('BackBundle:equerre:index.html.twig', array(
            'equerres' => $equerres,
        ));
    }

    /**
     * Creates a new equerre entity.
     *
     * @Route("/new", name="equerre_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $equerre = new Equerre();
        $form = $this->createForm('BackBundle\Form\EquerreType', $equerre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($equerre);
            $em->flush($equerre);

            return $this->redirectToRoute('equerre_show', array('id' => $equerre->getId()));
        }

        return $this->render('BackBundle:equerre:new.html.twig', array(
            'equerre' => $equerre,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a equerre entity.
     *
     * @Route("/{id}", name="equerre_show")
     * @Method("GET")
     */
    public function showAction(Equerre $equerre)
    {
        $deleteForm = $this->createDeleteForm($equerre);

        return $this->render('BackBundle:equerre:show.html.twig', array(
            'equerre' => $equerre,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing equerre entity.
     *
     * @Route("/{id}/edit", name="equerre_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Equerre $equerre)
    {
        $deleteForm = $this->createDeleteForm($equerre);
        $editForm = $this->createForm('BackBundle\Form\EquerreType', $equerre);
        $editForm->handleRequest($request);

        /* images */
        $em = $this->getDoctrine()->getManager();
        $images = $em->getRepository('BackBundle:Photo')->findAll();
        $couleurs = $em->getRepository('BackBundle:Couleur')->findAll();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('equerre_edit', array('id' => $equerre->getId()));
        }

        return $this->render('BackBundle:equerre:editCustom.html.twig', array(
            'images' => $images,
            'couleurs' => $couleurs,
            'equerre' => $equerre,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a equerre entity.
     *
     * @Route("/suppression/{id}", name="equerre_delete")
     * @Method({"GET", "DELETE"})
     */
    public function deleteAction(Request $request, Equerre $equerre)
    {
    $em = $this->getDoctrine()->getManager();
    $em->remove($equerre);
    $em->flush();

        return $this->redirectToRoute('equerre_index');
    }

    /**
     * Creates a form to delete a equerre entity.
     *
     * @param Equerre $equerre The equerre entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Equerre $equerre)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('equerre_delete', array('id' => $equerre->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Add Image
     * @Route("/addImage/{idEquerre}/{idPhoto}/{isMain}", name="equerre_addImage")
     * @ParamConverter("equerre", options={"mapping": {"idEquerre": "id"}})
     * @ParamConverter("photo", options={"mapping": {"idPhoto": "id"}})
     */
    public function addImage(Equerre $equerre,Photo $photo, $isMain)
    {
        $em = $this->getDoctrine()->getManager();
        $photo->setEquerre($equerre);
        $photo->setEstPrincipal($isMain);
        $em->persist($photo);
        $em->flush();
        return $this->redirectToRoute("equerre_edit", array("id"=>$equerre->getId()));
    }

    /**
     * Add Image
     * @Route("/addImage/{idEquerre}/{idPhoto}", name="equerre_removeImage")
     * @ParamConverter("equerre", options={"mapping": {"idEquerre": "id"}})
     * @ParamConverter("photo", options={"mapping": {"idPhoto": "id"}})
     */
    public function removeImage(Equerre $equerre,Photo $photo)
    {
        $em = $this->getDoctrine()->getManager();
        $photo->setEquerre(null);
        $em->persist($photo);
        $em->flush();
        return $this->redirectToRoute("equerre_edit", array("id"=>$equerre->getId()));
    }
}
