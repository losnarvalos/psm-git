<?php

namespace BackBundle\Controller;

use BackBundle\BackBundle;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use BackBundle\Entity\Planche;
use BackBundle\Entity\Equerre;
use BackBundle\Entity\Vis;
use BackBundle\Entity\Roue;
use BackBundle\Entity\Divers;

class StatController extends Controller
{
    /**
     * @Route("/listeProduitStockAlert", name="liste_produit_stock_alert")
     */
    public function listeProduitStockAlertAction()
    {
        $produits = $this->get('stockManager')->listeProduitStockAlert();
        $nb= $this->get('stockManager')->nbOrder($produits);
        return $this->render('BackBundle:Stat:liste_produit_stock_alerte.html.twig', array(
            'produits' => $produits,
            'nb_commande'=> $nb
        ));
    }

    /**
     * @Route("/listeProduitStockMini", name="liste_produit_stock_mini")
     */
    public function listeProduitStockMiniAction()
    {
        $produits = $this->get('stockManager')->listeProduitStockMini();
        $nb= $this->get('stockManager')->nbOrder($produits);
        return $this->render('BackBundle:Stat:liste_produit_stock_mini.html.twig', array(
            'produits' => $produits,
            'nb_commande'=> $nb
        ));
    }

    /**
     * @Route("/listeProduitStockOk", name="liste_produit_stock_ok")
     */
    public function listeProduitStockOkAction()
    {
        $produits = $this->get('stockManager')->listeProduitStockOk();
        return $this->render('BackBundle:Stat:liste_produit_stock_ok.html.twig', array(
            'produits' => $produits
        ));
    }

}
