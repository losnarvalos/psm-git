<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Photo;
use BackBundle\Entity\Roue;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Roue controller.
 *
 * @Route("roue")
 */
class RoueController extends Controller
{
    /**
     * Lists all roue entities.
     *
     * @Route("/", name="roue_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $roues = $em->getRepository('BackBundle:Roue')->findAll();

        return $this->render('BackBundle:roue:index.html.twig', array(
            'roues' => $roues,
        ));
    }

    /**
     * Creates a new roue entity.
     *
     * @Route("/new", name="roue_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $roue = new Roue();
        $form = $this->createForm('BackBundle\Form\RoueType', $roue);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($roue);
            $em->flush($roue);

            return $this->redirectToRoute('roue_show', array('id' => $roue->getId()));
        }

        return $this->render('BackBundle:roue:new.html.twig', array(
            'roue' => $roue,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a roue entity.
     *
     * @Route("/{id}", name="roue_show")
     * @Method("GET")
     */
    public function showAction(Roue $roue)
    {
        $deleteForm = $this->createDeleteForm($roue);

        return $this->render('BackBundle:roue:show.html.twig', array(
            'roue' => $roue,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
 * Displays a form to edit an existing roue entity.
 *
 * @Route("/{id}/edit", name="roue_edit")
 * @Method({"GET", "POST"})
 */
    public function editAction(Request $request, Roue $roue)
    {
        $deleteForm = $this->createDeleteForm($roue);
        $editForm = $this->createForm('BackBundle\Form\RoueType', $roue);
        $editForm->handleRequest($request);
        /* images */
        $em = $this->getDoctrine()->getManager();
        $images = $em->getRepository('BackBundle:Photo')->findAll();
        $couleurs = $em->getRepository('BackBundle:Couleur')->findAll();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('roue_edit', array('id' => $roue->getId()));
        }

        return $this->render('BackBundle:roue:editCustom.html.twig', array(
            'images' => $images,
            'couleurs' => $couleurs,
            'roue' => $roue,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Add Image
     * @Route("/addImage/{idRoue}/{idPhoto}/{isMain}", name="roue_addImage")
     * @ParamConverter("roue", options={"mapping": {"idRoue": "id"}})
     * @ParamConverter("photo", options={"mapping": {"idPhoto": "id"}})
     */
    public function addImage(Roue $roue,Photo $photo, $isMain)
    {
        $em = $this->getDoctrine()->getManager();
        $photo->setRoue($roue);
        $photo->setEstPrincipal($isMain);
        $em->persist($photo);
        $em->flush();
        return $this->redirectToRoute("roue_edit", array("id"=>$roue->getId()));
    }

    /**
     * Add Image
     * @Route("/addImage/{idRoue}/{idPhoto}", name="roue_removeImage")
     * @ParamConverter("roue", options={"mapping": {"idRoue": "id"}})
     * @ParamConverter("photo", options={"mapping": {"idPhoto": "id"}})
     */
    public function removeImage(Roue $roue,Photo $photo)
    {
        $em = $this->getDoctrine()->getManager();
        $photo->setRoue(null);
        $em->persist($photo);
        $em->flush();
        return $this->redirectToRoute("roue_edit", array("id"=>$roue->getId()));
    }


    /**
     * Deletes a roue entity.
     *
     * @Route("/suppression/{id}", name="roue_delete")
     * @Method({"GET", "DELETE"})
     */
    public function deleteAction(Request $request, Roue $roue)
    {
    $em = $this->getDoctrine()->getManager();
    $em->remove($roue);
    $em->flush();

        return $this->redirectToRoute('roue_index');
    }

    /**
     * Creates a form to delete a roue entity.
     *
     * @param Roue $roue The roue entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Roue $roue)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('roue_delete', array('id' => $roue->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
