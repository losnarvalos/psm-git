<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Materiau;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Materiau controller.
 *
 * @Route("materiau")
 */
class MateriauController extends Controller
{
    /**
     * Lists all materiau entities.
     *
     * @Route("/", name="materiau_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $materiaus = $em->getRepository('BackBundle:Materiau')->findAll();

        return $this->render('BackBundle:materiau:index.html.twig', array(
            'materiaus' => $materiaus,
        ));
    }

    /**
     * Creates a new materiau entity.
     *
     * @Route("/new", name="materiau_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $materiau = new Materiau();
        $form = $this->createForm('BackBundle\Form\MateriauType', $materiau);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($materiau);
            $em->flush($materiau);

            return $this->redirectToRoute('materiau_show', array('id' => $materiau->getId()));
        }

        return $this->render('BackBundle:materiau:new.html.twig', array(
            'materiau' => $materiau,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a materiau entity.
     *
     * @Route("/{id}", name="materiau_show")
     * @Method("GET")
     */
    public function showAction(Materiau $materiau)
    {
        $deleteForm = $this->createDeleteForm($materiau);

        return $this->render('BackBundle:materiau:show.html.twig', array(
            'materiau' => $materiau,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing materiau entity.
     *
     * @Route("/{id}/edit", name="materiau_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Materiau $materiau)
    {
        $deleteForm = $this->createDeleteForm($materiau);
        $editForm = $this->createForm('BackBundle\Form\MateriauType', $materiau);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('materiau_edit', array('id' => $materiau->getId()));
        }

        return $this->render('BackBundle:materiau:edit.html.twig', array(
            'materiau' => $materiau,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a materiau entity.
     *
     * @Route("/{id}", name="materiau_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Materiau $materiau)
    {
        $form = $this->createDeleteForm($materiau);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($materiau);
            $em->flush($materiau);
        }

        return $this->redirectToRoute('materiau_index');
    }

    /**
     * Creates a form to delete a materiau entity.
     *
     * @param Materiau $materiau The materiau entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Materiau $materiau)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('materiau_delete', array('id' => $materiau->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
