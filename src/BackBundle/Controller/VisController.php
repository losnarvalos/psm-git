<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Photo;
use BackBundle\Entity\Vis;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Vi controller.
 *
 * @Route("vis")
 */
class VisController extends Controller
{
    /**
     * Lists all vi entities.
     *
     * @Route("/", name="vis_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $vis = $em->getRepository('BackBundle:Vis')->findAll();

        return $this->render('BackBundle:vis:index.html.twig', array(
            'vis' => $vis,
        ));
    }

    /**
     * Creates a new vi entity.
     *
     * @Route("/new", name="vis_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $vi = new Vis();
        $form = $this->createForm('BackBundle\Form\VisType', $vi);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($vi);
            $em->flush($vi);

            return $this->redirectToRoute('vis_show', array('id' => $vi->getId()));
        }

        return $this->render('BackBundle:vis:new.html.twig', array(
            'vi' => $vi,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a vi entity.
     *
     * @Route("/{id}", name="vis_show")
     * @Method("GET")
     */
    public function showAction(Vis $vi)
    {
        $deleteForm = $this->createDeleteForm($vi);

        return $this->render('BackBundle:vis:show.html.twig', array(
            'vi' => $vi,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing vi entity.
     *
     * @Route("/{id}/edit", name="vis_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Vis $vi)
    {
        $deleteForm = $this->createDeleteForm($vi);
        $editForm = $this->createForm('BackBundle\Form\VisType', $vi);
        $editForm->handleRequest($request);

        /* images */
        $em = $this->getDoctrine()->getManager();
        $images = $em->getRepository('BackBundle:Photo')->findAll();
        $couleurs = $em->getRepository('BackBundle:Couleur')->findAll();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('vis_edit', array('id' => $vi->getId()));
        }

        return $this->render('BackBundle:vis:editCustom.html.twig', array(
            'images' => $images,
            'couleurs' => $couleurs,
            'vis' => $vi,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a vi entity.
     *
     * @Route("/suppression/{id}", name="vis_delete")
     * @Method({"GET", "DELETE"})
     */
    public function deleteAction(Request $request, Vis $vi)
    {
    $em = $this->getDoctrine()->getManager();
    $em->remove($vi);
    $em->flush();

        return $this->redirectToRoute('vis_index');
    }

    /**
     * Creates a form to delete a vi entity.
     *
     * @param Vis $vi The vi entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Vis $vi)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('vis_delete', array('id' => $vi->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


    /**
     * Add Image
     * @Route("/addImage/{idVis}/{idPhoto}/{isMain}", name="vis_addImage")
     * @ParamConverter("vis", options={"mapping": {"idVis": "id"}})
     * @ParamConverter("photo", options={"mapping": {"idPhoto": "id"}})
     */
    public function addImage(Vis $vis,Photo $photo, $isMain)
    {
        $em = $this->getDoctrine()->getManager();
        $photo->setVis($vis);
        $photo->setEstPrincipal($isMain);
        $em->persist($photo);
        $em->flush();
        return $this->redirectToRoute("vis_edit", array("id"=>$vis->getId()));
    }

    /**
     * Add Image
     * @Route("/addImage/{idVis}/{idPhoto}", name="vis_removeImage")
     * @ParamConverter("vis", options={"mapping": {"idVis": "id"}})
     * @ParamConverter("photo", options={"mapping": {"idPhoto": "id"}})
     */
    public function removeImage(Vis $vis,Photo $photo)
    {
        $em = $this->getDoctrine()->getManager();
        $photo->setVis(null);
        $em->persist($photo);
        $em->flush();
        return $this->redirectToRoute("vis_edit", array("id"=>$vis->getId()));
    }
}
