<?php

namespace BackBundle\Controller;

use BackBundle\Entity\TypeVissage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Typevissage controller.
 *
 * @Route("typevissage")
 */
class TypeVissageController extends Controller
{
    /**
     * Lists all typeVissage entities.
     *
     * @Route("/", name="typevissage_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $typeVissages = $em->getRepository('BackBundle:TypeVissage')->findAll();

        return $this->render('BackBundle:typevissage:index.html.twig', array(
            'typeVissages' => $typeVissages,
        ));
    }

    /**
     * Creates a new typeVissage entity.
     *
     * @Route("/new", name="typevissage_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $typeVissage = new Typevissage();
        $form = $this->createForm('BackBundle\Form\TypeVissageType', $typeVissage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typeVissage);
            $em->flush($typeVissage);

            return $this->redirectToRoute('typevissage_show', array('id' => $typeVissage->getId()));
        }

        return $this->render('BackBundle:typevissage:new.html.twig', array(
            'typeVissage' => $typeVissage,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a typeVissage entity.
     *
     * @Route("/{id}", name="typevissage_show")
     * @Method("GET")
     */
    public function showAction(TypeVissage $typeVissage)
    {
        $deleteForm = $this->createDeleteForm($typeVissage);

        return $this->render('BackBundle:typevissage:show.html.twig', array(
            'typeVissage' => $typeVissage,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing typeVissage entity.
     *
     * @Route("/{id}/edit", name="typevissage_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TypeVissage $typeVissage)
    {
        $deleteForm = $this->createDeleteForm($typeVissage);
        $editForm = $this->createForm('BackBundle\Form\TypeVissageType', $typeVissage);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('typevissage_edit', array('id' => $typeVissage->getId()));
        }

        return $this->render('BackBundle:typevissage:edit.html.twig', array(
            'typeVissage' => $typeVissage,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a typeVissage entity.
     *
     * @Route("/{id}", name="typevissage_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TypeVissage $typeVissage)
    {
        $form = $this->createDeleteForm($typeVissage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typeVissage);
            $em->flush($typeVissage);
        }

        return $this->redirectToRoute('typevissage_index');
    }

    /**
     * Creates a form to delete a typeVissage entity.
     *
     * @param TypeVissage $typeVissage The typeVissage entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TypeVissage $typeVissage)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('typevissage_delete', array('id' => $typeVissage->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
