<?php

namespace BackBundle\Controller;

use BackBundle\Entity\TypeDivers;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * TypeDivers controller.
 *
 * @Route("typedivers")
 */
class TypeDiversController extends Controller
{
    /**
     * Lists all typeDiver entities.
     *
     * @Route("/", name="typedivers_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $typeDivers = $em->getRepository('BackBundle:TypeDivers')->findAll();

        return $this->render('BackBundle:typedivers:index.html.twig', array(
            'typeDivers' => $typeDivers,
        ));
    }

    /**
     * Creates a new typeDiver entity.
     *
     * @Route("/new", name="typedivers_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $typeDiver = new TypeDivers();
        $form = $this->createForm('BackBundle\Form\TypeDiversType', $typeDiver);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typeDiver);
            $em->flush($typeDiver);

            return $this->redirectToRoute('typedivers_show', array('id' => $typeDiver->getId()));
        }

        return $this->render('BackBundle:typedivers:new.html.twig', array(
            'typeDiver' => $typeDiver,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a typeDiver entity.
     *
     * @Route("/{id}", name="typedivers_show")
     * @Method("GET")
     */
    public function showAction(TypeDivers $typeDiver)
    {
        $deleteForm = $this->createDeleteForm($typeDiver);

        return $this->render('BackBundle:typedivers:show.html.twig', array(
            'typeDiver' => $typeDiver,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing typeDiver entity.
     *
     * @Route("/{id}/edit", name="typedivers_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TypeDivers $typeDiver)
    {
        $deleteForm = $this->createDeleteForm($typeDiver);
        $editForm = $this->createForm('BackBundle\Form\TypeDiversType', $typeDiver);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('typedivers_edit', array('id' => $typeDiver->getId()));
        }

        return $this->render('BackBundle:typedivers:edit.html.twig', array(
            'typeDiver' => $typeDiver,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a typeDiver entity.
     *
     * @Route("/{id}", name="typedivers_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TypeDivers $typeDiver)
    {
        $form = $this->createDeleteForm($typeDiver);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typeDiver);
            $em->flush($typeDiver);
        }

        return $this->redirectToRoute('typedivers_index');
    }

    /**
     * Creates a form to delete a typeDiver entity.
     *
     * @param TypeDivers $typeDiver The typeDiver entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TypeDivers $typeDiver)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('typedivers_delete', array('id' => $typeDiver->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
