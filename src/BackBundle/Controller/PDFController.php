<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Command;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use BackBundle\Services\PDFManager;
use BackBundle\Services\StockManager;
use BackBundle\Entity\LigneCommand;

/**
 * PDF controller.
 *
 * @Route("PDF")
 */

class PDFController extends Controller
{
    /**
     * @Route("/pdfForDede/{id}" , name= "pdfForDede")
     *
     * @Method("GET")
     */
    public function generateDedePDFAction(Command $command)
    {
        $fileName = $this->get('PDFManager')->generateDedePDF($command);
        return new BinaryFileResponse($fileName);
    }

    /**
     * @Route("/pdfForProvider" , name= "pdfForProvider")
     *
     * @Method("GET")
     */
    public function generateProviderPDFAction()
    {
        $produits = $this->get('PDFManager')->getAllProductsForCustomers();
        
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $command = new Command();
        $command->setUser($user);
        $command->setState(0);
        $command->setType(1);
        foreach ($produits as $produit)
        {
            foreach ($produit as $item)
            {
                if ($this->get('StockManager')->isInCurrentOrder($item->getProduitGenerique()) == false)
                {
                    $ligneCommand = new LigneCommand();
                    $ligneCommand->setCommand($command);
                    $ligneCommand->setProduitGenerique($item->getProduitGenerique());
                    $ligneCommand->setQte($item->getNbCommandes());
                    $command->addLignesCommand($ligneCommand);
                    $em->persist($ligneCommand);
                }
            }
        }
        $em->persist($command);
        $em->flush();

        $fileName = $this->get('PDFManager')->generateProviderPDF();
        return new BinaryFileResponse($fileName);
    }
}
