<?php

namespace BackBundle\Controller;

use BackBundle\Entity\TypeFiletage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Typefiletage controller.
 *
 * @Route("typefiletage")
 */
class TypeFiletageController extends Controller
{
    /**
     * Lists all typeFiletage entities.
     *
     * @Route("/", name="typefiletage_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $typeFiletages = $em->getRepository('BackBundle:TypeFiletage')->findAll();

        return $this->render('BackBundle:typefiletage:index.html.twig', array(
            'typeFiletages' => $typeFiletages,
        ));
    }

    /**
     * Creates a new typeFiletage entity.
     *
     * @Route("/new", name="typefiletage_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $typeFiletage = new Typefiletage();
        $form = $this->createForm('BackBundle\Form\TypeFiletageType', $typeFiletage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typeFiletage);
            $em->flush($typeFiletage);

            return $this->redirectToRoute('typefiletage_show', array('id' => $typeFiletage->getId()));
        }

        return $this->render('BackBundle:typefiletage:new.html.twig', array(
            'typeFiletage' => $typeFiletage,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a typeFiletage entity.
     *
     * @Route("/{id}", name="typefiletage_show")
     * @Method("GET")
     */
    public function showAction(TypeFiletage $typeFiletage)
    {
        $deleteForm = $this->createDeleteForm($typeFiletage);

        return $this->render('BackBundle:typefiletage:show.html.twig', array(
            'typeFiletage' => $typeFiletage,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing typeFiletage entity.
     *
     * @Route("/{id}/edit", name="typefiletage_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TypeFiletage $typeFiletage)
    {
        $deleteForm = $this->createDeleteForm($typeFiletage);
        $editForm = $this->createForm('BackBundle\Form\TypeFiletageType', $typeFiletage);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('typefiletage_edit', array('id' => $typeFiletage->getId()));
        }

        return $this->render('BackBundle:typefiletage:edit.html.twig', array(
            'typeFiletage' => $typeFiletage,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a typeFiletage entity.
     *
     * @Route("/{id}", name="typefiletage_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TypeFiletage $typeFiletage)
    {
        $form = $this->createDeleteForm($typeFiletage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typeFiletage);
            $em->flush($typeFiletage);
        }

        return $this->redirectToRoute('typefiletage_index');
    }

    /**
     * Creates a form to delete a typeFiletage entity.
     *
     * @param TypeFiletage $typeFiletage The typeFiletage entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TypeFiletage $typeFiletage)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('typefiletage_delete', array('id' => $typeFiletage->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
