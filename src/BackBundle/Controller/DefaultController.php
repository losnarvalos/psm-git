<?php

namespace BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use BackBundle\Services\AfficheCouleur;
use BackBundle\Entity\Couleur;

class DefaultController extends Controller
{
    /**
     * @Route("/" , name= "backoffice")
     */
    public function indexAction()
    {
        $produits = $this->get('stockManager')->listeProduitStockAlert();
        $produitsTampons = $this->get('stockManager')->listeProduitStockMini();
        $produitsOk = $this->get('stockManager')->listeProduitStockOk();
        $tabAlerte = $this->get('stockManager')->getCount($produits);
        $tabTampon = $this->get('stockManager')->getCount($produitsTampons);
        $tabTotalOk = $this->get('stockManager')->getCount($produitsOk);

        $totalAlert = (array_sum($tabAlerte));
        $totalMini = (array_sum($tabTampon));
        $totalOk = array_sum($tabTotalOk);

        return $this->render('BackBundle:Default:index.html.twig', array(
            'nb_stock_alert_vis'        => $tabAlerte[0],
            'nb_stock_alert_planche'    => $tabAlerte[1],
            'nb_stock_alert_roue'       => $tabAlerte[2],
            'nb_stock_alert_equerre'    => $tabAlerte[3],
            'nb_stock_alert_divers'     => $tabAlerte[4],
            'nb_stock_tampon_vis'       => $tabTampon[0],
            'nb_stock_tampon_planche'   => $tabTampon[1],
            'nb_stock_tampon_roue'      => $tabTampon[2],
            'nb_stock_tampon_equerre'   => $tabTampon[3],
            'nb_stock_tampon_divers'    => $tabTampon[4],
            'total_alert'               => $totalAlert,
            'total_mini'                 => $totalMini,
            'total_ok'                  => $totalOk
        ));
    }

    /**
     * Add Image
     * @Route("/addCouleur/{class}/{obj}/{idCouleur}", name="addCouleur")
     * @ParamConverter("couleur", options={"mapping": {"idCouleur": "id"}})
     */
    public function addCouleur($class, $obj, Couleur $couleur)
    {
        $afficheCouleur = $this->get("afficheCouleur");
        $afficheCouleur -> attrCouleur($class,$obj,$couleur);

        return $this->redirectToRoute($class."_edit", array("id"=>$obj));
    }

    public function alertAction()
    {
        $produits = $this->get('stockManager')->listeProduitStockAlert();
        $produitsTampons = $this->get('stockManager')->listeProduitStockMini();
        
        $tabAlerte = $this->get('stockManager')->getCount($produits);
        $tabTampon = $this->get('stockManager')->getCount($produitsTampons);
        
        $totalAlert = (array_sum($tabAlerte));
        $totalMini = (array_sum($tabTampon));

        $nb = 0;

        if($totalAlert > 0 && $totalMini > 0){
            $nb = 2;
        }else if($totalAlert > 0 || $totalMini > 0){
            $nb = 1;
        }
        return $this->render('BackBundle:layout:cart.html.twig', array(
            'total_alert'               => $totalAlert,
            'total_mini'                => $totalMini,
            'count'                     => $nb
        ));
    }
}
