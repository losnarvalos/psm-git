<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Command;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;

/**
 * Command controller.
 *
 * @Route("command")
 */
class CommandController extends Controller
{
    /**
     * Lists all command entities.
     *
     * @Route("/", name="command_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $commands = $em->getRepository('BackBundle:Command')->findBy(array("type"=>0));

        return $this->render('BackBundle:command:index.html.twig', array(
            'commands' => $commands,
        ));
    }
    
    /**
     * Lists all command provider entities.
     *
     * @Route("/provider", name="commandProvider_index")
     * @Method("GET")
     */
    public function indexProviderAction()
    {
        $em = $this->getDoctrine()->getManager();

        $commands = $em->getRepository('BackBundle:Command')->findBy(array("type"=>1));

        return $this->render('BackBundle:command:indexProvider.html.twig', array(
            'commands' => $commands,
        ));
    }

    /**
     * Finds and displays a command entity.
     *
     * @Route("/{id}", name="command_show")
     * @Method("GET")
     */
    public function showAction(Command $command)
    {
        $deleteForm = $this->createDeleteForm($command);

        return $this->render('BackBundle:command:show.html.twig', array(
            'command' => $command,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing command entity.
     *
     * @Route("/{id}/edit", name="command_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Command $command)
    {
        $deleteForm = $this->createDeleteForm($command);
        $editForm = $this->createForm('BackBundle\Form\CommandType', $command);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('command_edit', array('id' => $command->getId()));
        }

        return $this->render('BackBundle:command:edit.html.twig', array(
            'command' => $command,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    /**
     * @Route("/changeState/{id}/{state}", name="change-state")
     */
    public function changeStateAction(Command $command, $state)
    {
        if ($command->getType() < 1){
            $redirect = 'command_index';
        }else{
            $redirect = 'commandProvider_index';
        }

        if (!is_numeric($state) || $state > 2) {
            $this->addFlash('warning', 'Statut non disponible');
            return $this->redirectToRoute($redirect);
        }

        $command->setState($state);

        $em = $this->getDoctrine()->getManager();
        $em->persist($command);
        $em->flush();

        if ($state==2 && $command->getType() == 1){
            $this->get('stockmanager')->resetStock($command);
        }

        return $this->redirectToRoute($redirect);
    }

    /**
     * Deletes a command entity.
     *
     * @Route("/suppression/{id}", name="command_delete")
     * @Method({"GET", "DELETE"})
     */
    public function deleteAction(Request $request, Command $command)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($command);
        $em->flush();

        if ($command->getType() < 1){
            return $this->redirectToRoute('command_index');
        }else{
            return $this->redirectToRoute('commandProvider_index');
        }
    }

    /**
     * Creates a form to delete a command entity.
     *
     * @param Command $command The command entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Command $command)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('command_delete', array('id' => $command->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }


    /**
     * Finds and displays commands by user.
     *
     * @Route("/commandByUser/{id}", name="commandByUser_show")
     * @Method("POST")
     */
    public function showCommandByUserAction(Request $request, User $user)
    {
        if($user == null){
            exit();
        }
        if($request->isXmlHttpRequest()){
            $em = $this->getDoctrine()->getManager();
            $commands = $em->getRepository('BackBundle:Command')->findByUser($user);

            return $this->render('BackBundle:command:commandByUser.html.twig', array(
                'commands' => $commands,
            ));
        }else{

        }
    }


}
