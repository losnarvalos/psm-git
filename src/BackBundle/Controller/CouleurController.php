<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Couleur;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Couleur controller.
 *
 * @Route("couleur")
 */
class CouleurController extends Controller
{
    /**
     * Lists all couleur entities.
     *
     * @Route("/", name="couleur_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $couleurs = $em->getRepository('BackBundle:Couleur')->findAll();

        return $this->render('BackBundle:couleur:index.html.twig', array(
            'couleurs' => $couleurs,
        ));
    }

    /**
     * Creates a new couleur entity.
     *
     * @Route("/new", name="couleur_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $couleur = new Couleur();
        $form = $this->createForm('BackBundle\Form\CouleurType', $couleur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($couleur);
            $em->flush($couleur);

            return $this->redirectToRoute('couleur_show', array('id' => $couleur->getId()));
        }

        return $this->render('BackBundle:couleur:new.html.twig', array(
            'couleur' => $couleur,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a couleur entity.
     *
     * @Route("/{id}", name="couleur_show")
     * @Method("GET")
     */
    public function showAction(Couleur $couleur)
    {
        $deleteForm = $this->createDeleteForm($couleur);

        return $this->render('BackBundle:couleur:show.html.twig', array(
            'couleur' => $couleur,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing couleur entity.
     *
     * @Route("/{id}/edit", name="couleur_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Couleur $couleur)
    {
        $deleteForm = $this->createDeleteForm($couleur);
        $editForm = $this->createForm('BackBundle\Form\CouleurType', $couleur);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('couleur_edit', array('id' => $couleur->getId()));
        }

        return $this->render('BackBundle:couleur:edit.html.twig', array(
            'couleur' => $couleur,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a couleur entity.
     *
     * @Route("/suppression/{id}", name="couleur_delete")
     * @Method({"GET", "DELETE"})
     */
    public function deleteAction(Request $request, Couleur $couleur)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($couleur);
        $em->flush();

        return $this->redirectToRoute('couleur_index');
    }

    /**
     * Creates a form to delete a couleur entity.
     *
     * @param Couleur $couleur The couleur entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Couleur $couleur)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('couleur_delete', array('id' => $couleur->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
