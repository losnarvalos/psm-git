<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Photo;
use BackBundle\Entity\Planche;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Planche controller.
 *
 * @Route("planche")
 */
class PlancheController extends Controller
{
    /**
     * Lists all planche entities.
     *
     * @Route("/", name="planche_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $planches = $em->getRepository('BackBundle:Planche')->findAll();

        return $this->render('BackBundle:planche:index.html.twig', array(
            'planches' => $planches,
        ));
    }

    /**
     * Creates a new planche entity.
     *
     * @Route("/new", name="planche_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $planche = new Planche();
        $form = $this->createForm('BackBundle\Form\PlancheType', $planche);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($planche);
            $em->flush($planche);

            return $this->redirectToRoute('planche_show', array('id' => $planche->getId()));
        }

        return $this->render('BackBundle:planche:new.html.twig', array(
            'planche' => $planche,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a planche entity.
     *
     * @Route("/{id}", name="planche_show")
     * @Method("GET")
     */
    public function showAction(Planche $planche)
    {
        $deleteForm = $this->createDeleteForm($planche);

        return $this->render('BackBundle:planche:show.html.twig', array(
            'planche' => $planche,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing planche entity.
     *
     * @Route("/{id}/edit", name="planche_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Planche $planche)
    {
        $deleteForm = $this->createDeleteForm($planche);
        $editForm = $this->createForm('BackBundle\Form\PlancheType', $planche);
        $editForm->handleRequest($request);

        /* images */
        $em = $this->getDoctrine()->getManager();
        $images = $em->getRepository('BackBundle:Photo')->findAll();
        $couleurs = $em->getRepository('BackBundle:Couleur')->findAll();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('planche_edit', array('id' => $planche->getId()));
        }

        return $this->render('BackBundle:planche:editCustom.html.twig', array(
            'images' => $images,
            'planche' => $planche,
            'couleurs' => $couleurs,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a planche entity.
     *
     * @Route("/suppression/{id}", name="planche_delete")
     * @Method({"GET", "DELETE"})
     */
    public function deleteAction(Request $request, Planche $planche)
    {
    $em = $this->getDoctrine()->getManager();
    $em->remove($planche);
    $em->flush();

        return $this->redirectToRoute('planche_index');
    }

    /**
     * Creates a form to delete a planche entity.
     *
     * @param Planche $planche The planche entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Planche $planche)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('planche_delete', array('id' => $planche->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


    /**
     * Add Image
     * @Route("/addImage/{idPlanche}/{idPhoto}/{isMain}", name="planche_addImage")
     * @ParamConverter("planche", options={"mapping": {"idPlanche": "id"}})
     * @ParamConverter("photo", options={"mapping": {"idPhoto": "id"}})
     */
    public function addImage(Planche $planche,Photo $photo, $isMain)
    {
        $em = $this->getDoctrine()->getManager();
        $photo->setPlanche($planche);
        $photo->setEstPrincipal($isMain);
        $em->persist($photo);
        $em->flush();
        return $this->redirectToRoute("planche_edit", array("id"=>$planche->getId()));
    }

    /**
     * Add Image
     * @Route("/addImage/{idPlanche}/{idPhoto}", name="planche_removeImage")
     * @ParamConverter("planche", options={"mapping": {"idPlanche": "id"}})
     * @ParamConverter("photo", options={"mapping": {"idPhoto": "id"}})
     */
    public function removeImage(Planche $planche,Photo $photo)
    {
        $em = $this->getDoctrine()->getManager();
        $photo->setPlanche(null);
        $em->persist($photo);
        $em->flush();
        return $this->redirectToRoute("planche_edit", array("id"=>$planche->getId()));
    }

}
