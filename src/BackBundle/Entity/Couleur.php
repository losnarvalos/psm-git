<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Couleur
 *
 * @ORM\Table(name="couleur")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\CouleurRepository")
 */
class Couleur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="valeur", type="string", length=255)
     */
    private $valeur;

    /**
     * @var string
     *
     * @ORM\Column(name="hexa", type="string", length=255)
     */
    private $hexa;


    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="Roue", mappedBy="couleur")
     */
    private $roues;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="Equerre", mappedBy="couleur")
     */
    private $equerres;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="Divers", mappedBy="couleur")
     */
    private $divers;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="Planche", mappedBy="couleur")
     */
    private $planches;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="Vis", mappedBy="couleur")
     */
    private $vis;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valeur
     *
     * @param string $valeur
     * @return Couleur
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;

        return $this;
    }

    /**
     * Get valeur
     *
     * @return string
     */
    public function getValeur()
    {
        return $this->valeur;
    }

    /**
     * Get hexa
     *
     * @return string
     */
    public function getHexa()
    {
        return $this->hexa;
    }

    public function __toString(){
        return $this->valeur;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roues = new \Doctrine\Common\Collections\ArrayCollection();
        $this->equerres = new \Doctrine\Common\Collections\ArrayCollection();
        $this->divers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->planches = new \Doctrine\Common\Collections\ArrayCollection();
        $this->vis = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set hexa
     *
     * @param string $hexa
     * @return Couleur
     */
    public function setHexa($hexa)
    {
        $this->hexa = $hexa;

        return $this;
    }

    /**
     * Add roues
     *
     * @param \BackBundle\Entity\Roue $roues
     * @return Couleur
     */
    public function addRoue(\BackBundle\Entity\Roue $roues)
    {
        $this->roues[] = $roues;

        return $this;
    }

    /**
     * Remove roues
     *
     * @param \BackBundle\Entity\Roue $roues
     */
    public function removeRoue(\BackBundle\Entity\Roue $roues)
    {
        $this->roues->removeElement($roues);
    }

    /**
     * Get roues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoues()
    {
        return $this->roues;
    }

    /**
     * Add equerres
     *
     * @param \BackBundle\Entity\Equerre $equerres
     * @return Couleur
     */
    public function addEquerre(\BackBundle\Entity\Equerre $equerres)
    {
        $this->equerres[] = $equerres;

        return $this;
    }

    /**
     * Remove equerres
     *
     * @param \BackBundle\Entity\Equerre $equerres
     */
    public function removeEquerre(\BackBundle\Entity\Equerre $equerres)
    {
        $this->equerres->removeElement($equerres);
    }

    /**
     * Get equerres
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEquerres()
    {
        return $this->equerres;
    }

    /**
     * Add divers
     *
     * @param \BackBundle\Entity\Divers $divers
     * @return Couleur
     */
    public function addDiver(\BackBundle\Entity\Divers $divers)
    {
        $this->divers[] = $divers;

        return $this;
    }

    /**
     * Remove divers
     *
     * @param \BackBundle\Entity\Divers $divers
     */
    public function removeDiver(\BackBundle\Entity\Divers $divers)
    {
        $this->divers->removeElement($divers);
    }

    /**
     * Get divers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDivers()
    {
        return $this->divers;
    }

    /**
     * Add planches
     *
     * @param \BackBundle\Entity\Planche $planches
     * @return Couleur
     */
    public function addPlanch(\BackBundle\Entity\Planche $planches)
    {
        $this->planches[] = $planches;

        return $this;
    }

    /**
     * Remove planches
     *
     * @param \BackBundle\Entity\Planche $planches
     */
    public function removePlanch(\BackBundle\Entity\Planche $planches)
    {
        $this->planches->removeElement($planches);
    }

    /**
     * Get planches
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlanches()
    {
        return $this->planches;
    }

    /**
     * Add vis
     *
     * @param \BackBundle\Entity\Vis $vis
     * @return Couleur
     */
    public function addVi(\BackBundle\Entity\Vis $vis)
    {
        $this->vis[] = $vis;

        return $this;
    }

    /**
     * Remove vis
     *
     * @param \BackBundle\Entity\Vis $vis
     */
    public function removeVi(\BackBundle\Entity\Vis $vis)
    {
        $this->vis->removeElement($vis);
    }

    /**
     * Get vis
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVis()
    {
        return $this->vis;
    }
}
