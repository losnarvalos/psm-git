<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LigneCommand
 *
 * @ORM\Table(name="ligne_command")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\LigneCommandRepository")
 */
class LigneCommand
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="qte", type="integer")
     */
    private $qte;

    /**
     * Many Features have One Command.
     * @ORM\ManyToOne(targetEntity="Command", inversedBy="lignes_command")
     */
    private $command;

    /**
     * Many Features have One Product.
     * @ORM\ManyToOne(targetEntity="ProduitGenerique", inversedBy="ligneCommand")
     */
    private $produitGenerique;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qte
     *
     * @param integer $qte
     * @return LigneCommand
     */
    public function setQte($qte)
    {
        $this->qte = $qte;

        return $this;
    }

    /**
     * Get qte
     *
     * @return integer 
     */
    public function getQte()
    {
        return $this->qte;
    }

    /**
     * Set command
     *
     * @param \BackBundle\Entity\Command $command
     * @return LigneCommand
     */
    public function setCommand(\BackBundle\Entity\Command $command = null)
    {
        $this->command = $command;

        return $this;
    }

    /**
     * Get command
     *
     * @return \BackBundle\Entity\Command 
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * Set produitGenerique
     *
     * @param \BackBundle\Entity\ProduitGenerique $produitGenerique
     * @return LigneCommand
     */
    public function setProduitGenerique(\BackBundle\Entity\ProduitGenerique $produitGenerique = null)
    {
        $this->produitGenerique = $produitGenerique;

        return $this;
    }

    /**
     * Get produitGenerique
     *
     * @return \BackBundle\Entity\ProduitGenerique 
     */
    public function getProduitGenerique()
    {
        return $this->produitGenerique;
    }
}
