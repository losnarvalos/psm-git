<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Essence
 *
 * @ORM\Table(name="essence")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\EssenceRepository")
 */
class Essence
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;


    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="Planche", mappedBy="essence")
     */
    private $planche;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Essence
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Essence
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function __toString()
    {
        return $this->libelle;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->planche = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add planche
     *
     * @param \BackBundle\Entity\Planche $planche
     * @return Essence
     */
    public function addPlanche(\BackBundle\Entity\Planche $planche)
    {
        $this->planche[] = $planche;

        return $this;
    }

    /**
     * Remove planche
     *
     * @param \BackBundle\Entity\Planche $planche
     */
    public function removePlanche(\BackBundle\Entity\Planche $planche)
    {
        $this->planche->removeElement($planche);
    }

    /**
     * Get planche
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlanche()
    {
        return $this->planche;
    }
}
