<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LignePanier
 *
 * @ORM\Table(name="ligne_panier")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\LignePanierRepository")
 */
class LignePanier
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="qte", type="integer")
     */
    private $qte;

    /**
     * Many Features have One Product.
     * @ORM\ManyToOne(targetEntity="Panier", inversedBy="ligne_panier")
     * @ORM\JoinColumn(name="panier_id", referencedColumnName="id")
     */
    private $panier;

    /**
     * Many Features have One Product.
     * @ORM\ManyToOne(targetEntity="ProduitGenerique", inversedBy="lignePanier")
     * @ORM\JoinColumn(name="produit_generique_id", referencedColumnName="id")
     */
    private $produitGenerique;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qte
     *
     * @param integer $qte
     * @return LignePanier
     */
    public function setQte($qte)
    {
        $this->qte = $qte;

        return $this;
    }

    /**
     * Get qte
     *
     * @return integer 
     */
    public function getQte()
    {
        return $this->qte;
    }

    /**
     * Set panier
     *
     * @param integer $panier
     * @return LignePanier
     */
    public function setPanier($panier)
    {
        $this->panier = $panier;

        return $this;
    }

    /**
     * Get panier
     *
     * @return integer 
     */
    public function getPanier()
    {
        return $this->panier;
    }



    /**
     * Set produitGenerique
     *
     * @param \BackBundle\Entity\ProduitGenerique $produitGenerique
     * @return LignePanier
     */
    public function setProduitGenerique(\BackBundle\Entity\ProduitGenerique $produitGenerique = null)
    {
        $this->produitGenerique = $produitGenerique;

        return $this;
    }

    /**
     * Get produitGenerique
     *
     * @return \BackBundle\Entity\ProduitGenerique 
     */
    public function getProduitGenerique()
    {
        return $this->produitGenerique;
    }
}
