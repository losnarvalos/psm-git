<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Panier
 *
 * @ORM\Table(name="commande")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\CommandRepository")
 */
class Command
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many Command has One Customer.
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="command")
     */
    private $user;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="LigneCommand", mappedBy="command", cascade={"remove"})
     */
    private $lignes_command;

    /**
     * @var int
     *
     * @ORM\Column(name="state", type="integer")
     */
    private $state;

    /**
     * @var boolean
     *
     * @ORM\Column(name="type", type="boolean")
     */
    private $type;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lignes_command = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     * @return Command
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add lignes_command
     *
     * @param \BackBundle\Entity\LigneCommand $lignesCommand
     * @return Command
     */
    public function addLignesCommand(\BackBundle\Entity\LigneCommand $lignesCommand)
    {
        $this->lignes_command[] = $lignesCommand;

        return $this;
    }

    /**
     * Remove lignes_command
     *
     * @param \BackBundle\Entity\LigneCommand $lignesCommand
     */
    public function removeLignesCommand(\BackBundle\Entity\LigneCommand $lignesCommand)
    {
        $this->lignes_command->removeElement($lignesCommand);
    }

    /**
     * Get lignes_command
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLignesCommand()
    {
        return $this->lignes_command;
    }

    /**
     * Set state
     *
     * @param integer $state
     * @return Command
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Get state format
     *
     * @return integer
     */
    public function getStateHtml()
    {
        switch ($this->state)
        {
            case '2':
                return '<i class="fa fa-ticket fa-2x" aria-hidden="true" style="color:green"></i>';
                break;
            case '1':
                return '<i class="fa fa-ticket fa-2x" aria-hidden="true" style="color:orange"></i>';
                break;
            default :
                return '<i class="fa fa-ticket fa-2x" aria-hidden="true" style="color:red"></i>';
        }
    }

    public function getPrixTotal() {
        $lignes = $this->getLignesCommand();
        $prixTotal = 0;
        foreach ($lignes as $ligne) {
            $prixTotal += $ligne->getProduitGenerique()->getProduit()->getPrix()*$ligne->getQte();
        }

        return $prixTotal;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Command
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Command
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set type
     *
     * @param boolean $type
     * @return Command
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return boolean 
     */
    public function getType()
    {
        return $this->type;
    }
}
