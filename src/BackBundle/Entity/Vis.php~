<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vis
 *
 * @ORM\Table(name="vis")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\VisRepository")
 */
class Vis
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255)
     */
    private $reference;
    
    /**
     * @var float
     *
     * @ORM\Column(name="poids", type="float")
     */
    private $poids;

    /**
     * @var float
     *
     * @ORM\Column(name="longueur", type="float")
     */
    private $longueur;

    /**
     * @var float
     *
     * @ORM\Column(name="diametre_tete", type="float")
     */
    private $diametre_tete;

    /**
     * @var float
     *
     * @ORM\Column(name="diametre_filetage", type="float")
     */
    private $diametre_filetage;


    /**
     * @var string
     *
     * @ORM\Column(name="emplacement", type="string", length=255)
     */
    private $emplacement;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float")
     */
    private $prix;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     *
     * @ORM\ManyToOne(targetEntity="TypeTete", inversedBy="vis")
     */
    private $typetete;

    /**
     *
     * @ORM\ManyToOne(targetEntity="TypeFiletage", inversedBy="vis")
     */
    private $typefiletage;

    /**
     *
     * @ORM\ManyToOne(targetEntity="TypeVissage", inversedBy="vis")
     */
    private $typevissage;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Couleur", inversedBy="vis")
     */
    private $couleur;

    /**
     *
     * @ORM\OneToMany(targetEntity="Photo", mappedBy="vis")
     */
    private $photos;

    /**
     * @var string
     *
     * @ORM\Column(name="taille", type="string")
     */
    private $taille;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_commandes", type="integer")
     */
    private $nb_commandes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_reapro_dernier", type="datetime")
     */
    private $date_reapro_dernier;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_reapro_prochain", type="datetime")
     */
    private $date_reapro_prochain;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock_mini", type="integer")
     */
    private $stock_mini;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock_alerte", type="integer")
     */
    private $stock_alerte;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock", type="integer")
     */
    private $stock;

    /**
     * @ORM\OneToOne(targetEntity="ProduitGenerique", mappedBy="vis")
     */
    private $produitGenerique;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Vis
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Vis
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set longueur
     *
     * @param float $longueur
     * @return Vis
     */
    public function setLongueur($longueur)
    {
        $this->longueur = $longueur;

        return $this;
    }

    /**
     * Get longueur
     *
     * @return float
     */
    public function getLongueur()
    {
        return $this->longueur;
    }




    public function getClassName(){
        return 'Vis';
    }

    public function getPhotoPrincipale(){
        $res = "";
        foreach ($this->photos as $photo){
            if ($photo->getEstPrincipal() === true){
                $res = $photo;
            }
        }
        return $res;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->photos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get couleurs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCouleurs()
    {
        return $this->couleurs;
    }

    public function __toString(){
        return 'vs'.$this->reference;
    }

    /**
     * Set couleur
     *
     * @param \BackBundle\Entity\Couleur $couleur
     * @return Vis
     */
    public function setCouleur(\BackBundle\Entity\Couleur $couleur = null)
    {
        $this->couleur = $couleur;

        return $this;
    }

    /**
     * Get couleur
     *
     * @return \BackBundle\Entity\Couleur
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * Add photos
     *
     * @param \BackBundle\Entity\Photo $photos
     * @return Vis
     */
    public function addPhoto(\BackBundle\Entity\Photo $photos)
    {
        $this->photos[] = $photos;

        return $this;
    }

    /**
     * Remove photos
     *
     * @param \BackBundle\Entity\Photo $photos
     */
    public function removePhoto(\BackBundle\Entity\Photo $photos)
    {
        $this->photos->removeElement($photos);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Get photoNonPrincipal
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotosNonPrincipal()
    {
        $res=[];
        foreach($this->photos as $photo) {
            if(!$photo->getEstPrincipal()){
                $res[] = $photo;
            }
        }
        return $res;
    }

  

    /**
     * Set typetete
     *
     * @param \BackBundle\Entity\TypeTete $typetete
     * @return Vis
     */
    public function setTypetete(\BackBundle\Entity\TypeTete $typetete = null)
    {
        $this->typetete = $typetete;

        return $this;
    }

    /**
     * Get typetete
     *
     * @return \BackBundle\Entity\TypeTete 
     */
    public function getTypetete()
    {
        return $this->typetete;
    }

    /**
     * Set typefiletage
     *
     * @param \BackBundle\Entity\TypeFiletage $typefiletage
     * @return Vis
     */
    public function setTypefiletage(\BackBundle\Entity\TypeFiletage $typefiletage = null)
    {
        $this->typefiletage = $typefiletage;

        return $this;
    }

    /**
     * Get typefiletage
     *
     * @return \BackBundle\Entity\TypeFiletage 
     */
    public function getTypefiletage()
    {
        return $this->typefiletage;
    }

    /**
     * Set typevissage
     *
     * @param \BackBundle\Entity\TypeVissage $typevissage
     * @return Vis
     */
    public function setTypevissage(\BackBundle\Entity\TypeVissage $typevissage = null)
    {
        $this->typevissage = $typevissage;

        return $this;
    }

    /**
     * Get typevissage
     *
     * @return \BackBundle\Entity\TypeVissage 
     */
    public function getTypevissage()
    {
        return $this->typevissage;
    }

    /**
     * Set poids
     *
     * @param float $poids
     * @return Vis
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * Get poids
     *
     * @return float 
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * Set diametre_tete
     *
     * @param float $diametreTete
     * @return Vis
     */
    public function setDiametreTete($diametreTete)
    {
        $this->diametre_tete = $diametreTete;

        return $this;
    }

    /**
     * Get diametre_tete
     *
     * @return float 
     */
    public function getDiametreTete()
    {
        return $this->diametre_tete;
    }

    /**
     * Set diametre_filetage
     *
     * @param float $diametreFiletage
     * @return Vis
     */
    public function setDiametreFiletage($diametreFiletage)
    {
        $this->diametre_filetage = $diametreFiletage;

        return $this;
    }

    /**
     * Get diametre_filetage
     *
     * @return float 
     */
    public function getDiametreFiletage()
    {
        return $this->diametre_filetage;
    }

    /**
     * Set emplacement
     *
     * @param string $emplacement
     * @return Vis
     */
    public function setEmplacement($emplacement)
    {
        $this->emplacement = $emplacement;

        return $this;
    }

    /**
     * Get emplacement
     *
     * @return string 
     */
    public function getEmplacement()
    {
        return $this->emplacement;
    }

    /**
     * Set prix
     *
     * @param float $prix
     * @return Vis
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Vis
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set taille
     *
     * @param string $taille
     * @return Vis
     */
    public function setTaille($taille)
    {
        $this->taille = $taille;

        return $this;
    }

    /**
     * Get taille
     *
     * @return string 
     */
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * Get nb_commandes
     *
     * @return \int 
     */
    public function getNbCommandes()
    {
        return $this->nb_commandes;
    }

    /**
     * Set date_reapro_dernier
     *
     * @param \DateTime $dateReaproDernier
     * @return Vis
     */
    public function setDateReaproDernier($dateReaproDernier)
    {
        $this->date_reapro_dernier = $dateReaproDernier;

        return $this;
    }

    /**
     * Get date_reapro_dernier
     *
     * @return \DateTime 
     */
    public function getDateReaproDernier()
    {
        return $this->date_reapro_dernier;
    }

    /**
     * Set date_reapro_prochain
     *
     * @param \DateTime $dateReaproProchain
     * @return Vis
     */
    public function setDateReaproProchain($dateReaproProchain)
    {
        $this->date_reapro_prochain = $dateReaproProchain;

        return $this;
    }

    /**
     * Get date_reapro_prochain
     *
     * @return \DateTime 
     */
    public function getDateReaproProchain()
    {
        return $this->date_reapro_prochain;
    }

    /**
     * Set nb_commandes
     *
     * @param integer $nbCommandes
     * @return Vis
     */
    public function setNbCommandes($nbCommandes)
    {
        $this->nb_commandes = $nbCommandes;

        return $this;
    }

    /**
     * Set stock_mini
     *
     * @param integer $stockMini
     * @return Vis
     */
    public function setStockMini($stockMini)
    {
        $this->stock_mini = $stockMini;

        return $this;
    }

    /**
     * Get stock_mini
     *
     * @return integer 
     */
    public function getStockMini()
    {
        return $this->stock_mini;
    }

    /**
     * Set stock_alerte
     *
     * @param integer $stockAlerte
     * @return Vis
     */
    public function setStockAlerte($stockAlerte)
    {
        $this->stock_alerte = $stockAlerte;

        return $this;
    }

    /**
     * Get stock_alerte
     *
     * @return integer 
     */
    public function getStockAlerte()
    {
        return $this->stock_alerte;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     * @return Vis
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer 
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set produitGenerique
     *
     * @param \BackBundle\Entity\ProduitGenerique $produitGenerique
     * @return Vis
     */
    public function setProduitGenerique(\BackBundle\Entity\ProduitGenerique $produitGenerique = null)
    {
        $this->produitGenerique = $produitGenerique;

        return $this;
    }

    /**
     * Get produitGenerique
     *
     * @return \BackBundle\Entity\ProduitGenerique 
     */
    public function getProduitGenerique()
    {
        return $this->produitGenerique;
    }
}
