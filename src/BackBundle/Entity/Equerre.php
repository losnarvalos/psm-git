<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Equerre
 *
 * @ORM\Table(name="equerre")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\EquerreRepository")
 */
class Equerre
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255)
     */
    private $reference;

    /**
     * @var float
     *
     * @ORM\Column(name="longueur_min", type="float")
     */
    private $longueur_min;

    /**
     * @var float
     *
     * @ORM\Column(name="longueur_max", type="float")
     */
    private $longueur_max;

    /**
     * @var float
     *
     * @ORM\Column(name="angle", type="float")
     */
    private $angle;


    /**
     * @var float
     *
     * @ORM\Column(name="poids", type="float")
     */
    private $poids;

    /**
     * @var string
     *
     * @ORM\Column(name="emplacement", type="string", length=255)
     */
    private $emplacement;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float")
     */
    private $prix;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Couleur", inversedBy="equerres")
     */
    private $couleur;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Materiau", inversedBy="equerre")
     */
    private $materiau;

    /**
     *
     * @ORM\OneToMany(targetEntity="Photo", mappedBy="equerre")
     */
    private $photos;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_trous", type="integer")
     */
    private $nb_trous;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock_mini", type="integer")
     */
    private $stock_mini;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock_alerte", type="integer")
     */
    private $stock_alerte;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock", type="integer")
     */
    private $stock;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_commandes", type="integer")
     */
    private $nb_commandes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_reapro_dernier", type="datetime")
     */
    private $date_reapro_dernier;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_reapro_prochain", type="datetime")
     */
    private $date_reapro_prochain;

    /**
     * @ORM\OneToOne(targetEntity="ProduitGenerique", mappedBy="equerre")
     */
    private $produitGenerique;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->photos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getClassName(){
        return 'Equerre';
    }

    public function getPhotoPrincipale(){
        $res = "";
        foreach ($this->photos as $photo){
            if ($photo->getEstPrincipal() === true){
                $res = $photo;
            }
        }
        return $res;
    }

    /**
     * Set poids
     *
     * @param float $poids
     * @return Equerre
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * Get poids
     *
     * @return float
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Equerre
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function __toString(){
        return 'eq'.$this->reference;
    }

    /**
     * Set couleur
     *
     * @param \BackBundle\Entity\Couleur $couleur
     * @return Equerre
     */
    public function setCouleur(\BackBundle\Entity\Couleur $couleur = null)
    {
        $this->couleur = $couleur;

        return $this;
    }

    /**
     * Get couleur
     *
     * @return \BackBundle\Entity\Couleur
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * Add photos
     *
     * @param \BackBundle\Entity\Photo $photos
     * @return Equerre
     */
    public function addPhoto(\BackBundle\Entity\Photo $photos)
    {
        $this->photos[] = $photos;

        return $this;
    }

    /**
     * Remove photos
     *
     * @param \BackBundle\Entity\Photo $photos
     */
    public function removePhoto(\BackBundle\Entity\Photo $photos)
    {
        $this->photos->removeElement($photos);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Get photoNonPrincipal
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotosNonPrincipal()
    {
        $res=[];
        foreach($this->photos as $photo) {
            if(!$photo->getEstPrincipal()){
                $res[] = $photo;
            }
        }
        return $res;
    }

    /**
     * Set materiau
     *
     * @param \BackBundle\Entity\Materiau $materiau
     * @return Equerre
     */
    public function setMateriau(\BackBundle\Entity\Materiau $materiau = null)
    {
        $this->materiau = $materiau;

        return $this;
    }

    /**
     * Get materiau
     *
     * @return \BackBundle\Entity\Materiau 
     */
    public function getMateriau()
    {
        return $this->materiau;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Equerre
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Equerre
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set longueur_min
     *
     * @param float $longueurMin
     * @return Equerre
     */
    public function setLongueurMin($longueurMin)
    {
        $this->longueur_min = $longueurMin;

        return $this;
    }

    /**
     * Get longueur_min
     *
     * @return float 
     */
    public function getLongueurMin()
    {
        return $this->longueur_min;
    }

    /**
     * Set longueur_max
     *
     * @param float $longueurMax
     * @return Equerre
     */
    public function setLongueurMax($longueurMax)
    {
        $this->longueur_max = $longueurMax;

        return $this;
    }

    /**
     * Get longueur_max
     *
     * @return float 
     */
    public function getLongueurMax()
    {
        return $this->longueur_max;
    }

    /**
     * Set angle
     *
     * @param float $angle
     * @return Equerre
     */
    public function setAngle($angle)
    {
        $this->angle = $angle;

        return $this;
    }

    /**
     * Get angle
     *
     * @return float 
     */
    public function getAngle()
    {
        return $this->angle;
    }

    /**
     * Set emplacement
     *
     * @param string $emplacement
     * @return Equerre
     */
    public function setEmplacement($emplacement)
    {
        $this->emplacement = $emplacement;

        return $this;
    }

    /**
     * Get emplacement
     *
     * @return string 
     */
    public function getEmplacement()
    {
        return $this->emplacement;
    }

    /**
     * Set prix
     *
     * @param float $prix
     * @return Equerre
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set nb_trous
     *
     * @param \int $nbTrous
     * @return Equerre
     */
    public function setNbTrous($nbTrous)
    {
        $this->nb_trous = $nbTrous;

        return $this;
    }

    /**
     * Get nb_trous
     *
     * @return \int 
     */
    public function getNbTrous()
    {
        return $this->nb_trous;
    }

    /**
     * Set stock_mini
     *
     * @param integer $stockMini
     * @return Equerre
     */
    public function setStockMini($stockMini)
    {
        $this->stock_mini = $stockMini;

        return $this;
    }

    /**
     * Get stock_mini
     *
     * @return integer
     */
    public function getStockMini()
    {
        return $this->stock_mini;
    }

    /**
     * Set stock_alerte
     *
     * @param integer $stockAlerte
     * @return Equerre
     */
    public function setStockAlerte($stockAlerte)
    {
        $this->stock_alerte = $stockAlerte;

        return $this;
    }

    /**
     * Get stock_alerte
     *
     * @return integer
     */
    public function getStockAlerte()
    {
        return $this->stock_alerte;
    }

    /**
     * Set nb_commandes
     *
     * @param \int $nbCommandes
     * @return Equerre
     */
    public function setNbCommandes($nbCommandes)
    {
        $this->nb_commandes = $nbCommandes;

        return $this;
    }

    /**
     * Get nb_commandes
     *
     * @return \int 
     */
    public function getNbCommandes()
    {
        return $this->nb_commandes;
    }

    /**
     * Set date_reapro_dernier
     *
     * @param \DateTime $dateReaproDernier
     * @return Equerre
     */
    public function setDateReaproDernier($dateReaproDernier)
    {
        $this->date_reapro_dernier = $dateReaproDernier;

        return $this;
    }

    /**
     * Get date_reapro_dernier
     *
     * @return \DateTime 
     */
    public function getDateReaproDernier()
    {
        return $this->date_reapro_dernier;
    }

    /**
     * Set date_reapro_prochain
     *
     * @param \DateTime $dateReaproProchain
     * @return Equerre
     */
    public function setDateReaproProchain($dateReaproProchain)
    {
        $this->date_reapro_prochain = $dateReaproProchain;

        return $this;
    }

    /**
     * Get date_reapro_prochain
     *
     * @return \DateTime 
     */
    public function getDateReaproProchain()
    {
        return $this->date_reapro_prochain;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     * @return Equerre
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer 
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set produitGenerique
     *
     * @param \BackBundle\Entity\ProduitGenerique $produitGenerique
     * @return Equerre
     */
    public function setProduitGenerique(\BackBundle\Entity\ProduitGenerique $produitGenerique = null)
    {
        $this->produitGenerique = $produitGenerique;

        return $this;
    }

    /**
     * Get produitGenerique
     *
     * @return \BackBundle\Entity\ProduitGenerique 
     */
    public function getProduitGenerique()
    {
        return $this->produitGenerique;
    }
}
