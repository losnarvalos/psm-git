<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProduitGenerique
 *
 * @ORM\Table(name="produit_generique")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\ProduitGeneriqueRepository")
 */
class ProduitGenerique
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Divers", inversedBy="produitGenerique")
     * @ORM\JoinColumn(nullable=true)
     */
    private $divers;

    /**
     * @ORM\OneToOne(targetEntity="Equerre", inversedBy="produitGenerique")
     * @ORM\JoinColumn(nullable=true)
     */
    private $equerre;

    /**
     * @ORM\OneToOne(targetEntity="Planche", inversedBy="produitGenerique")
     * @ORM\JoinColumn(nullable=true)
     */
    private $planche;

    /**
     * @ORM\OneToOne(targetEntity="Roue", inversedBy="produitGenerique")
     * @ORM\JoinColumn(nullable=true)
     */
    private $roue;

    /**
     * @ORM\OneToOne(targetEntity="Vis", inversedBy="produitGenerique")
     * @ORM\JoinColumn(nullable=true)
     */
    private $vis;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="LignePanier", mappedBy="produitGenerique")
     */
    private $lignePanier;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set roue
     *
     * @param \BackBundle\Entity\Roue $roue
     * @return ProduitGenerique
     */
    public function setRoue(\BackBundle\Entity\Roue $roue = null)
    {
        $this->roue = $roue;

        return $this;
    }

    /**
     * Get roue
     *
     * @return \BackBundle\Entity\Roue 
     */
    public function getRoue()
    {
        return $this->roue;
    }

    /**
     * Set divers
     *
     * @param \BackBundle\Entity\Divers $divers
     * @return ProduitGenerique
     */
    public function setDivers(\BackBundle\Entity\Divers $divers = null)
    {
        $this->divers = $divers;

        return $this;
    }

    /**
     * Get divers
     *
     * @return \BackBundle\Entity\Divers 
     */
    public function getDivers()
    {
        return $this->divers;
    }

    /**
     * Set equerre
     *
     * @param \BackBundle\Entity\Equerre $equerre
     * @return ProduitGenerique
     */
    public function setEquerre(\BackBundle\Entity\Equerre $equerre = null)
    {
        $this->equerre = $equerre;

        return $this;
    }

    /**
     * Get equerre
     *
     * @return \BackBundle\Entity\Equerre 
     */
    public function getEquerre()
    {
        return $this->equerre;
    }

    /**
     * Set planche
     *
     * @param \BackBundle\Entity\Planche $planche
     * @return ProduitGenerique
     */
    public function setPlanche(\BackBundle\Entity\Planche $planche = null)
    {
        $this->planche = $planche;

        return $this;
    }

    /**
     * Get planche
     *
     * @return \BackBundle\Entity\Planche 
     */
    public function getPlanche()
    {
        return $this->planche;
    }

    /**
     * Set vis
     *
     * @param \BackBundle\Entity\Vis $vis
     * @return ProduitGenerique
     */
    public function setVis(\BackBundle\Entity\Vis $vis = null)
    {
        $this->vis = $vis;

        return $this;
    }

    /**
     * Get vis
     *
     * @return \BackBundle\Entity\Vis 
     */
    public function getVis()
    {
        return $this->vis;
    }
    
    

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lignePanier = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add lignePanier
     *
     * @param \BackBundle\Entity\lignePanier $lignePanier
     * @return ProduitGenerique
     */
    public function addLignePanier(\BackBundle\Entity\lignePanier $lignePanier)
    {
        $this->lignePanier[] = $lignePanier;

        return $this;
    }

    /**
     * Remove lignePanier
     *
     * @param \BackBundle\Entity\lignePanier $lignePanier
     */
    public function removeLignePanier(\BackBundle\Entity\lignePanier $lignePanier)
    {
        $this->lignePanier->removeElement($lignePanier);
    }

    /**
     * Get lignePanier
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLignePanier()
    {
        return $this->lignePanier;
    }

    public function getProduit() {

        if ($this->getEquerre() != null ) {
            $produit = $this->getEquerre();
        }else if ($this->getDivers() != null ) {
            $produit = $this->getDivers();
        }else if ($this->getPlanche() != null ) {
            $produit = $this->getPlanche();
        }else if ($this->getRoue() != null ) {
            $produit = $this->getRoue();
        }else if ($this->getVis() != null ) {
            $produit = $this->getVis();
        }

        return $produit;
    }

    public function getType() {

        if ($this->getEquerre() != null ) {
            $type = 'Equerre';
        }else if ($this->getDivers() != null ) {
            $type = 'Divers';
        }else if ($this->getPlanche() != null ) {
            $type = 'Planche';
        }else if ($this->getRoue() != null ) {
            $type = 'Roue';
        }else if ($this->getVis() != null ) {
            $type = 'Vis';
        }

        return $type;
    }
}
