<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Materiau
 *
 * @ORM\Table(name="materiau")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\MateriauRepository")
 */
class Materiau
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="Equerre", mappedBy="materiau")
     */
    private $equerre;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Materiau
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Materiau
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }


    public function __toString()
    {
        return $this->libelle;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->equerre = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add equerre
     *
     * @param \BackBundle\Entity\Equerre $equerre
     * @return Materiau
     */
    public function addEquerre(\BackBundle\Entity\Equerre $equerre)
    {
        $this->equerre[] = $equerre;

        return $this;
    }

    /**
     * Remove equerre
     *
     * @param \BackBundle\Entity\Equerre $equerre
     */
    public function removeEquerre(\BackBundle\Entity\Equerre $equerre)
    {
        $this->equerre->removeElement($equerre);
    }

    /**
     * Get equerre
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEquerre()
    {
        return $this->equerre;
    }
}
