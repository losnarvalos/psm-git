<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypeVissage
 *
 * @ORM\Table(name="type_vissage")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\TypeVissageRepository")
 */
class TypeVissage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="Vis", mappedBy="typevissage")
     */
    private $vis;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return TypeVissage
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return TypeVissage
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function __toString()
    {
        return $this->libelle;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->vis = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add vis
     *
     * @param \BackBundle\Entity\Vis $vis
     * @return TypeVissage
     */
    public function addVi(\BackBundle\Entity\Vis $vis)
    {
        $this->vis[] = $vis;

        return $this;
    }

    /**
     * Remove vis
     *
     * @param \BackBundle\Entity\Vis $vis
     */
    public function removeVi(\BackBundle\Entity\Vis $vis)
    {
        $this->vis->removeElement($vis);
    }

    /**
     * Get vis
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVis()
    {
        return $this->vis;
    }
}
