<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Divers
 *
 * @ORM\Table(name="divers")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\DiversRepository")
 */
class Divers
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="emplacement", type="string", length=255)
     */
    private $emplacement;

    /**
     * @var text
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="taille", type="string")
     */
    private $taille;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock_mini", type="integer")
     */
    private $stock_mini;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock_alerte", type="integer")
     */
    private $stock_alerte;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock", type="integer")
     */
    private $stock;

    /**
     * @var float
     *
     * @ORM\Column(name="poids", type="float")
     */
    private $poids;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_commandes", type="integer")
     */
    private $nb_commandes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_reapro_dernier", type="datetime")
     */
    private $date_reapro_dernier;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_reapro_prochain", type="datetime")
     */
    private $date_reapro_prochain;


    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float")
     */
    private $prix;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Couleur", inversedBy="divers")
     */
    private $couleur;

    /**
     *
     * @ORM\OneToMany(targetEntity="Photo", mappedBy="divers")
     */
    private $photos;


    /**
     *
     * @ORM\ManyToOne(targetEntity="TypeDivers", inversedBy="divers")
     */
    private $type;

    /**
     * @ORM\OneToOne(targetEntity="ProduitGenerique", mappedBy="divers")
     */
    private $produitGenerique;

    /**
     * Divers constructor.
     */
    public function __construct()
    {
        $this->photos = new \Doctrine\Common\Collections\ArrayCollection();
    }





    public function getPhotoPrincipale()
    {
        $res=null;
        foreach($this->photos as $photo) {
            if($photo->getEstPrincipal()){
                $res=$photo;
            }
        }

        return $res;
    }

     /**
     * Get Classname
     *
     * @return string
     */
    public function getClassName()
    {
        return 'Divers';
    }

    public function __toString(){
        return 'dvs'.$this->reference;
    }

    /**
     * Set couleur
     *
     * @param \BackBundle\Entity\Couleur $couleur
     * @return Divers
     */
    public function setCouleur(\BackBundle\Entity\Couleur $couleur = null)
    {
        $this->couleur = $couleur;

        return $this;
    }

    /**
     * Get couleur
     *
     * @return \BackBundle\Entity\Couleur
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * Add photos
     *
     * @param \BackBundle\Entity\Photo $photos
     * @return Divers
     */
    public function addPhoto(\BackBundle\Entity\Photo $photos)
    {
        $this->photos[] = $photos;

        return $this;
    }

    /**
     * Remove photos
     *
     * @param \BackBundle\Entity\Photo $photos
     */
    public function removePhoto(\BackBundle\Entity\Photo $photos)
    {
        $this->photos->removeElement($photos);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Get photoNonPrincipal
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotosNonPrincipal()
    {
        $res=[];
        foreach($this->photos as $photo) {
            if(!$photo->getEstPrincipal()){
                $res[] = $photo;
            }
        }
        return $res;
    }

    /**
     * Set type
     *
     * @param \BackBundle\Entity\TypeDivers $type
     * @return Divers
     */
    public function setType(\BackBundle\Entity\TypeDivers $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \BackBundle\Entity\TypeDivers 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Divers
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Divers
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set emplacement
     *
     * @param string $emplacement
     * @return Divers
     */
    public function setEmplacement($emplacement)
    {
        $this->emplacement = $emplacement;

        return $this;
    }

    /**
     * Get emplacement
     *
     * @return string 
     */
    public function getEmplacement()
    {
        return $this->emplacement;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Divers
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set taille
     *
     * @param string $taille
     * @return Divers
     */
    public function setTaille($taille)
    {
        $this->taille = $taille;

        return $this;
    }

    /**
     * Get taille
     *
     * @return string 
     */
    public function getTaille()
    {
        return $this->taille;
    }
    
    /**
     * Set poids
     *
     * @param float $poids
     * @return Divers
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * Get poids
     *
     * @return float 
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * Set nb_commandes
     *
     * @param \int $nbCommandes
     * @return Divers
     */
    public function setNbCommandes( $nbCommandes)
    {
        $this->nb_commandes = $nbCommandes;

        return $this;
    }

    /**
     * Get nb_commandes
     *
     * @return \int 
     */
    public function getNbCommandes()
    {
        return $this->nb_commandes;
    }

    /**
     * Set date_reapro_dernier
     *
     * @param \DateTime $dateReaproDernier
     * @return Divers
     */
    public function setDateReaproDernier($dateReaproDernier)
    {
        $this->date_reapro_dernier = $dateReaproDernier;

        return $this;
    }

    /**
     * Get date_reapro_dernier
     *
     * @return \DateTime 
     */
    public function getDateReaproDernier()
    {
        return $this->date_reapro_dernier;
    }

    /**
     * Set date_reapro_prochain
     *
     * @param \DateTime $dateReaproProchain
     * @return Divers
     */
    public function setDateReaproProchain($dateReaproProchain)
    {
        $this->date_reapro_prochain = $dateReaproProchain;

        return $this;
    }

    /**
     * Get date_reapro_prochain
     *
     * @return \DateTime 
     */
    public function getDateReaproProchain()
    {
        return $this->date_reapro_prochain;
    }

    /**
     * Set prix
     *
     * @param float $prix
     * @return Divers
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->prix;
    }
    

    /**
     * Set stock_mini
     *
     * @param integer $stockMini
     * @return Divers
     */
    public function setStockMini($stockMini)
    {
        $this->stock_mini = $stockMini;

        return $this;
    }

    /**
     * Get stock_mini
     *
     * @return integer 
     */
    public function getStockMini()
    {
        return $this->stock_mini;
    }

    /**
     * Set stock_alerte
     *
     * @param integer $stockAlerte
     * @return Divers
     */
    public function setStockAlerte($stockAlerte)
    {
        $this->stock_alerte = $stockAlerte;

        return $this;
    }

    /**
     * Get stock_alerte
     *
     * @return integer 
     */
    public function getStockAlerte()
    {
        return $this->stock_alerte;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     * @return Divers
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer 
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set produitGenerique
     *
     * @param \BackBundle\Entity\ProduitGenerique $produitGenerique
     * @return Divers
     */
    public function setProduitGenerique(\BackBundle\Entity\ProduitGenerique $produitGenerique = null)
    {
        $this->produitGenerique = $produitGenerique;

        return $this;
    }

    /**
     * Get produitGenerique
     *
     * @return \BackBundle\Entity\ProduitGenerique 
     */
    public function getProduitGenerique()
    {
        return $this->produitGenerique;
    }
}
