<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Photo
 *
 * @ORM\Table(name="photo")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\PhotoRepository")
 * @Vich\Uploadable
 */
class Photo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estPrincipal", type="boolean", nullable=true)
     */
    private $estPrincipal;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="Equerre", inversedBy="photos")
     * @ORM\JoinColumn(name="equerre_id", referencedColumnName="id", nullable=true)
     */
    private $equerre;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="Roue", inversedBy="photos")
     * @ORM\JoinColumn(name="roue_id", referencedColumnName="id", nullable=true)
     */
    private $roue;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="Divers", inversedBy="photos")
     * @ORM\JoinColumn(name="divers_id", referencedColumnName="id", nullable=true)
     */
    private $divers;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="Vis", inversedBy="photos")
     * @ORM\JoinColumn(name="vis_id", referencedColumnName="id", nullable=true)
     */
    private $vis;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="Planche", inversedBy="photos")
     * @ORM\JoinColumn(name="planche_id", referencedColumnName="id", nullable=true)
     */
    private $planche;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="product_image", fileNameProperty="imageName", nullable=true)
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Product
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        if ($image  instanceof UploadedFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     *
     * @return Product
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Photo
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set estPrincipal
     *
     * @param integer $estPrincipal
     * @return Photo
     */
    public function setEstPrincipal($estPrincipal)
    {
        $this->estPrincipal = $estPrincipal;

        return $this;
    }

    /**
     * Get estPrincipal
     *
     * @return integer 
     */
    public function getEstPrincipal()
    {
        return $this->estPrincipal;
    }

    /**
     * Set equerre
     *
     * @param \BackBundle\Entity\Equerre $equerre
     * @return Photo
     */
    public function setEquerre(\BackBundle\Entity\Equerre $equerre = null)
    {
        $this->equerre = $equerre;

        return $this;
    }

    /**
     * Get equerre
     *
     * @return \BackBundle\Entity\Equerre 
     */
    public function getEquerre()
    {
        return $this->equerre;
    }

    /**
     * Set roue
     *
     * @param \BackBundle\Entity\Roue $roue
     * @return Photo
     */
    public function setRoue(\BackBundle\Entity\Roue $roue = null)
    {
        $this->roue = $roue;

        return $this;
    }

    /**
     * Get roue
     *
     * @return \BackBundle\Entity\Roue 
     */
    public function getRoue()
    {
        return $this->roue;
    }

    /**
     * Set divers
     *
     * @param \BackBundle\Entity\Divers $divers
     * @return Photo
     */
    public function setDivers(\BackBundle\Entity\Divers $divers = null)
    {
        $this->divers = $divers;

        return $this;
    }

    /**
     * Get divers
     *
     * @return \BackBundle\Entity\Divers 
     */
    public function getDivers()
    {
        return $this->divers;
    }

    /**
     * Set vis
     *
     * @param \BackBundle\Entity\Vis $vis
     * @return Photo
     */
    public function setVis(\BackBundle\Entity\Vis $vis = null)
    {
        $this->vis = $vis;

        return $this;
    }

    /**
     * Get vis
     *
     * @return \BackBundle\Entity\Vis 
     */
    public function getVis()
    {
        return $this->vis;
    }

    /**
     * Set planche
     *
     * @param \BackBundle\Entity\Planche $planche
     * @return Photo
     */
    public function setPlanche(\BackBundle\Entity\Planche $planche = null)
    {
        $this->planche = $planche;

        return $this;
    }

    /**
     * Get planche
     *
     * @return \BackBundle\Entity\Planche 
     */
    public function getPlanche()
    {
        return $this->planche;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Photo
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }


    public function __toString(){
        return $this->nom;
    }
}
