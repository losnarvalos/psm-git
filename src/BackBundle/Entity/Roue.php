<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Roue
 *
 * @ORM\Table(name="roue")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\RoueRepository")
 */
class Roue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="emplacement", type="string", length=255)
     */
    private $emplacement;

    /**
     * @var float
     *
     * @ORM\Column(name="diametre", type="float")
     */
    private $diametre;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float")
     */
    private $prix;

    /**
     * @var float
     *
     * @ORM\Column(name="poids", type="float")
     */
    private $poids;

    /**
     * @var text
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Couleur", inversedBy="roues")
     */
    private $couleur;
    /**
     *
     * @ORM\OneToMany(targetEntity="Photo", mappedBy="roue")
     */
    private $photos;

    /**
     * @var float
     *
     * @ORM\Column(name="hauteur_pneu", type="float")
     */
    private $hauteur_pneu;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_commandes", type="integer")
     */
    private $nb_commandes;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_boulons", type="integer")
     */
    private $nb_boulons;

    /**
     * @ORM\OneToOne(targetEntity="ProduitGenerique", mappedBy="roue")
     */
    private $produitGenerique;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock_mini", type="integer")
     */
    private $stock_mini;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock_alerte", type="integer")
     */
    private $stock_alerte;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock", type="integer")
     */
    private $stock;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_reapro_dernier", type="datetime")
     */
    private $date_reapro_dernier;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_reapro_prochain", type="datetime")
     */
    private $date_reapro_prochain;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->photos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get Classname
     *
     * @return string
     */
    public function getClassName()
    {
        return 'Roue';
    }

    /**
     * Get PhotoPrincipal
     *
     * @return string
     */
    public function getPhotoPrincipale()
    {
        $res = "";

        foreach ($this->photos as $photo){
            if ($photo -> getEstPrincipal() === true){
                $res  =  $photo;
            }
        }

        return $res;
    }



    public function __toString(){
        return 'r'.$this->reference;
    }

    /**
     * Set couleur
     *
     * @param \BackBundle\Entity\Couleur $couleur
     * @return Roue
     */
    public function setCouleur(\BackBundle\Entity\Couleur $couleur = null)
    {
        $this->couleur = $couleur;

        return $this;
    }

    /**
     * Get couleur
     *
     * @return \BackBundle\Entity\Couleur
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * Add photos
     *
     * @param \BackBundle\Entity\Photo $photos
     * @return Roue
     */
    public function addPhoto(\BackBundle\Entity\Photo $photos)
    {
        $this->photos[] = $photos;

        return $this;
    }

    /**
     * Remove photos
     *
     * @param \BackBundle\Entity\Photo $photos
     */
    public function removePhoto(\BackBundle\Entity\Photo $photos)
    {
        $this->photos->removeElement($photos);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Get photoNonPrincipal
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotosNonPrincipal()
    {
        $res=[];
        foreach($this->photos as $photo) {
            if(!$photo->getEstPrincipal()){
                $res[] = $photo;
            }
        }
        return $res;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Roue
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Roue
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set emplacement
     *
     * @param string $emplacement
     * @return Roue
     */
    public function setEmplacement($emplacement)
    {
        $this->emplacement = $emplacement;

        return $this;
    }

    /**
     * Get emplacement
     *
     * @return string 
     */
    public function getEmplacement()
    {
        return $this->emplacement;
    }

    /**
     * Set diametre
     *
     * @param float $diametre
     * @return Roue
     */
    public function setDiametre($diametre)
    {
        $this->diametre = $diametre;

        return $this;
    }

    /**
     * Get diametre
     *
     * @return float 
     */
    public function getDiametre()
    {
        return $this->diametre;
    }

    /**
     * Set prix
     *
     * @param float $prix
     * @return Roue
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set poids
     *
     * @param float $poids
     * @return Roue
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * Get poids
     *
     * @return float 
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Roue
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set hauteur_pneu
     *
     * @param float $hauteurPneu
     * @return Roue
     */
    public function setHauteurPneu($hauteurPneu)
    {
        $this->hauteur_pneu = $hauteurPneu;

        return $this;
    }

    /**
     * Get hauteur_pneu
     *
     * @return float 
     */
    public function getHauteurPneu()
    {
        return $this->hauteur_pneu;
    }

    /**
     * Set nb_boulons
     *
     * @param \int $nbBoulons
     * @return Roue
     */
    public function setNbBoulons($nbBoulons)
    {
        $this->nb_boulons = $nbBoulons;

        return $this;
    }

    /**
     * Get nb_boulons
     *
     * @return \int 
     */
    public function getNbBoulons()
    {
        return $this->nb_boulons;
    }

    /**
     * Set produitGenerique
     *
     * @param \BackBundle\Entity\ProduitGenerique $produitGenerique
     * @return Roue
     */
    public function setProduitGenerique(\BackBundle\Entity\ProduitGenerique $produitGenerique = null)
    {
        $this->produitGenerique = $produitGenerique;

        return $this;
    }

    /**
     * Get produitGenerique
     *
     * @return \BackBundle\Entity\ProduitGenerique 
     */
    public function getProduitGenerique()
    {
        return $this->produitGenerique;
    }

    /**
     * Set stock_mini
     *
     * @param integer $stockMini
     * @return Roue
     */
    public function setStockMini($stockMini)
    {
        $this->stock_mini = $stockMini;

        return $this;
    }

    /**
     * Get stock_mini
     *
     * @return integer 
     */
    public function getStockMini()
    {
        return $this->stock_mini;
    }

    /**
     * Set stock_alerte
     *
     * @param integer $stockAlerte
     * @return Roue
     */
    public function setStockAlerte($stockAlerte)
    {
        $this->stock_alerte = $stockAlerte;

        return $this;
    }

    /**
     * Get stock_alerte
     *
     * @return integer 
     */
    public function getStockAlerte()
    {
        return $this->stock_alerte;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     * @return Roue
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer 
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set nb_commandes
     *
     * @param integer $nbCommandes
     * @return Roue
     */
    public function setNbCommandes($nbCommandes)
    {
        $this->nb_commandes = $nbCommandes;

        return $this;
    }

    /**
     * Get nb_commandes
     *
     * @return integer 
     */
    public function getNbCommandes()
    {
        return $this->nb_commandes;
    }

    /**
     * Set date_reapro_dernier
     *
     * @param \DateTime $dateReaproDernier
     * @return Roue
     */
    public function setDateReaproDernier($dateReaproDernier)
    {
        $this->date_reapro_dernier = $dateReaproDernier;

        return $this;
    }

    /**
     * Get date_reapro_dernier
     *
     * @return \DateTime 
     */
    public function getDateReaproDernier()
    {
        return $this->date_reapro_dernier;
    }

    /**
     * Set date_reapro_prochain
     *
     * @param \DateTime $dateReaproProchain
     * @return Roue
     */
    public function setDateReaproProchain($dateReaproProchain)
    {
        $this->date_reapro_prochain = $dateReaproProchain;

        return $this;
    }

    /**
     * Get date_reapro_prochain
     *
     * @return \DateTime 
     */
    public function getDateReaproProchain()
    {
        return $this->date_reapro_prochain;
    }
}
