<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Panier
 *
 * @ORM\Table(name="panier")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\PanierRepository")
 */
class Panier
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * One Cart has One Customer.
     * @ORM\OneToOne(targetEntity="UserBundle\Entity\User", inversedBy="panier")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    // ...
    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="LignePanier", mappedBy="panier", cascade={"remove"})
     */
    private $ligne_panier;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param integer $user
     * @return Panier
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ligne_panier = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->getUser()->getEmail();
    }

    /**
     * Add ligne_panier
     *
     * @param \BackBundle\Entity\LignePanier $lignePanier
     * @return Panier
     */
    public function addLignePanier(\BackBundle\Entity\LignePanier $lignePanier)
    {
        $this->ligne_panier[] = $lignePanier;

        return $this;
    }

    /**
     * Remove ligne_panier
     *
     * @param \BackBundle\Entity\LignePanier $lignePanier
     */
    public function removeLignePanier(\BackBundle\Entity\LignePanier $lignePanier)
    {
        $this->ligne_panier->removeElement($lignePanier);
    }
    

    /**
     * Get ligne_panier
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLignePanier()
    {
        return $this->ligne_panier;
    }

    /**
     * Get ligne_panier by id
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLignePanierById($id)
    {
        $return = false;
        foreach ($this->ligne_panier as $ligne){
            if($ligne->getId() == $id){
                $return = $ligne;
            }
        }
        return $return;
    }

    public function getPrixTotal() {
        $lignes = $this->getLignePanier();
        $prixTotal = 0;
        foreach ($lignes as $ligne) {
            $prixTotal += $ligne->getProduitGenerique()->getProduit()->getPrix()*$ligne->getQte();
        }

        return $prixTotal;
    }
    
    public function resetPanier($em)
    {
        foreach ($this->ligne_panier as $ligne)
        {
            $em->remove($ligne);
        }
        $em->flush();
        return $this;
    }
}
