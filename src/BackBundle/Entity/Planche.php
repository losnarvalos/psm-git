<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Planche
 *
 * @ORM\Table(name="planche")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\PlancheRepository")
 */
class Planche
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255)
     */
    private $reference;

    /**
     * @var float
     *
     * @ORM\Column(name="longueur", type="float")
     */
    private $longueur;

    /**
     * @var float
     *
     * @ORM\Column(name="largeur", type="float")
     */
    private $largeur;

    /**
     * @var float
     *
     * @ORM\Column(name="epaisseur", type="float")
     */
    private $epaisseur;

    /**
     * @var string
     *
     * @ORM\Column(name="emplacement", type="string", length=255)
     */
    private $emplacement;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float")
     */
    private $prix;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Couleur", inversedBy="planches")
     */
    private $couleur;

    /**
     *
     * @ORM\OneToMany(targetEntity="Photo", mappedBy="planche")
     */
    private $photos;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Essence", inversedBy="planche")
     */
    private $essence;

    /**
     * @var float
     *
     * @ORM\Column(name="humidite", type="float")
     */
    private $humidite;

    /**
     * @var int
     *
     * @ORM\Column(name="durete", type="integer")
     */
    private $durete;

    /**
     * @var string
     *
     * @ORM\Column(name="taille", type="string")
     */
    private $taille;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock_mini", type="integer")
     */
    private $stock_mini;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock_alerte", type="integer")
     */
    private $stock_alerte;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock", type="integer")
     */
    private $stock;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_commandes", type="integer")
     */
    private $nb_commandes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_reapro_dernier", type="datetime")
     */
    private $date_reapro_dernier;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_reapro_prochain", type="datetime")
     */
    private $date_reapro_prochain;
    
    /**
     * @var float
     *
     * @ORM\Column(name="poids", type="float")
     */
    private $poids;

    /**
     * @ORM\OneToOne(targetEntity="ProduitGenerique", mappedBy="planche")
     */
    private $produitGenerique;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->photos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get Classname
     *
     * @return string
     */
    public function getClassName()
    {
        return 'Planche';
    }

    /**
     * Get PhotoPrincipal
     *
     * @return string
     */
    public function getPhotoPrincipale()
    {
        $res = "";

        foreach ($this->photos as $photo){
            if ($photo -> getEstPrincipal() === true){
                $res  =  $photo;
            }
        }

        return $res;
    }

    public function __toString(){
        return 'pl'.$this->reference;
    }

    /**
     * Set couleur
     *
     * @param \BackBundle\Entity\Couleur $couleur
     * @return Planche
     */
    public function setCouleur(\BackBundle\Entity\Couleur $couleur = null)
    {
        $this->couleur = $couleur;

        return $this;
    }

    /**
     * Get couleur
     *
     * @return \BackBundle\Entity\Couleur
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * Add photos
     *
     * @param \BackBundle\Entity\Photo $photos
     * @return Planche
     */
    public function addPhoto(\BackBundle\Entity\Photo $photos)
    {
        $this->photos[] = $photos;

        return $this;
    }

    /**
     * Remove photos
     *
     * @param \BackBundle\Entity\Photo $photos
     */
    public function removePhoto(\BackBundle\Entity\Photo $photos)
    {
        $this->photos->removeElement($photos);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Get photoNonPrincipal
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotosNonPrincipal()
    {
        $res=[];
        foreach($this->photos as $photo) {
            if(!$photo->getEstPrincipal()){
                $res[] = $photo;
            }
        }
        return $res;
    }

    /**
     * Set essence
     *
     * @param \BackBundle\Entity\Essence $essence
     * @return Planche
     */
    public function setEssence(\BackBundle\Entity\Essence $essence = null)
    {
        $this->essence = $essence;

        return $this;
    }

    /**
     * Get essence
     *
     * @return \BackBundle\Entity\Essence 
     */
    public function getEssence()
    {
        return $this->essence;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Planche
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Planche
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set longueur
     *
     * @param float $longueur
     * @return Planche
     */
    public function setLongueur($longueur)
    {
        $this->longueur = $longueur;

        return $this;
    }

    /**
     * Get longueur
     *
     * @return float 
     */
    public function getLongueur()
    {
        return $this->longueur;
    }

    /**
     * Set largeur
     *
     * @param float $largeur
     * @return Planche
     */
    public function setLargeur($largeur)
    {
        $this->largeur = $largeur;

        return $this;
    }

    /**
     * Get largeur
     *
     * @return float 
     */
    public function getLargeur()
    {
        return $this->largeur;
    }

    /**
     * Set epaisseur
     *
     * @param float $epaisseur
     * @return Planche
     */
    public function setEpaisseur($epaisseur)
    {
        $this->epaisseur = $epaisseur;

        return $this;
    }

    /**
     * Get epaisseur
     *
     * @return float 
     */
    public function getEpaisseur()
    {
        return $this->epaisseur;
    }

    /**
     * Set emplacement
     *
     * @param string $emplacement
     * @return Planche
     */
    public function setEmplacement($emplacement)
    {
        $this->emplacement = $emplacement;

        return $this;
    }

    /**
     * Get emplacement
     *
     * @return string 
     */
    public function getEmplacement()
    {
        return $this->emplacement;
    }

    /**
     * Set prix
     *
     * @param float $prix
     * @return Planche
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Planche
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set humidite
     *
     * @param float $humidite
     * @return Planche
     */
    public function setHumidite($humidite)
    {
        $this->humidite = $humidite;

        return $this;
    }

    /**
     * Get humidite
     *
     * @return float 
     */
    public function getHumidite()
    {
        return $this->humidite;
    }

    /**
     * Set durete
     *
     * @param \int $durete
     * @return Planche
     */
    public function setDurete($durete)
    {
        $this->durete = $durete;

        return $this;
    }

    /**
     * Get durete
     *
     * @return \int 
     */
    public function getDurete()
    {
        return $this->durete;
    }

    /**
     * Set taille
     *
     * @param string $taille
     * @return Planche
     */
    public function setTaille($taille)
    {
        $this->taille = $taille;

        return $this;
    }

    /**
     * Get taille
     *
     * @return string 
     */
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * Set nb_commandes
     *
     * @param \int $nbCommandes
     * @return Planche
     */
    public function setNbCommandes($nbCommandes)
    {
        $this->nb_commandes = $nbCommandes;

        return $this;
    }

    /**
     * Get nb_commandes
     *
     * @return \int 
     */
    public function getNbCommandes()
    {
        return $this->nb_commandes;
    }

    /**
     * Set date_reapro_dernier
     *
     * @param \DateTime $dateReaproDernier
     * @return Planche
     */
    public function setDateReaproDernier($dateReaproDernier)
    {
        $this->date_reapro_dernier = $dateReaproDernier;

        return $this;
    }

    /**
     * Get date_reapro_dernier
     *
     * @return \DateTime 
     */
    public function getDateReaproDernier()
    {
        return $this->date_reapro_dernier;
    }

    /**
     * Set date_reapro_prochain
     *
     * @param \DateTime $dateReaproProchain
     * @return Planche
     */
    public function setDateReaproProchain($dateReaproProchain)
    {
        $this->date_reapro_prochain = $dateReaproProchain;

        return $this;
    }

    /**
     * Get date_reapro_prochain
     *
     * @return \DateTime 
     */
    public function getDateReaproProchain()
    {
        return $this->date_reapro_prochain;
    }

    /**
     * Set poids
     *
     * @param float $poids
     * @return Planche
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * Get poids
     *
     * @return float 
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * Set stock_mini
     *
     * @param integer $stockMini
     * @return Planche
     */
    public function setStockMini($stockMini)
    {
        $this->stock_mini = $stockMini;

        return $this;
    }

    /**
     * Get stock_mini
     *
     * @return integer 
     */
    public function getStockMini()
    {
        return $this->stock_mini;
    }

    /**
     * Set stock_alerte
     *
     * @param integer $stockAlerte
     * @return Planche
     */
    public function setStockAlerte($stockAlerte)
    {
        $this->stock_alerte = $stockAlerte;

        return $this;
    }

    /**
     * Get stock_alerte
     *
     * @return integer 
     */
    public function getStockAlerte()
    {
        return $this->stock_alerte;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     * @return Planche
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer 
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set produitGenerique
     *
     * @param \BackBundle\Entity\ProduitGenerique $produitGenerique
     * @return Planche
     */
    public function setProduitGenerique(\BackBundle\Entity\ProduitGenerique $produitGenerique = null)
    {
        $this->produitGenerique = $produitGenerique;

        return $this;
    }

    /**
     * Get produitGenerique
     *
     * @return \BackBundle\Entity\ProduitGenerique 
     */
    public function getProduitGenerique()
    {
        return $this->produitGenerique;
    }
}
