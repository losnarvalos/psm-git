<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypeDivers
 *
 * @ORM\Table(name="type_divers")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\TypeDiversRepository")
 */
class TypeDivers
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255, nullable=true)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="Divers", mappedBy="type")
     */
    private $divers;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return TypeDivers
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return TypeDivers
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->divers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add divers
     *
     * @param \BackBundle\Entity\Divers $divers
     * @return TypeDivers
     */
    public function addDiver(\BackBundle\Entity\Divers $divers)
    {
        $this->divers[] = $divers;

        return $this;
    }

    /**
     * Remove divers
     *
     * @param \BackBundle\Entity\Divers $divers
     */
    public function removeDiver(\BackBundle\Entity\Divers $divers)
    {
        $this->divers->removeElement($divers);
    }

    /**
     * Get divers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDivers()
    {
        return $this->divers;
    }

    public function __toString()
    {
        return $this->libelle;
    }
}
