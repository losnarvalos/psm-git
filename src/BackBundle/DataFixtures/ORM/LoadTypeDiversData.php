<?php
// src/OC/PlatformBundle/DataFixtures/ORM/LoadRoueData.php

namespace BackBundle\DataFixtures\ORM;
use BackBundle\Entity\TypeDivers;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadTypeDiverData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Liste des noms de compétences à ajouter
        // On crée la compétence
        $obj = new TypeDivers();
        $obj->setLibelle("Type divers test");
        $obj->setDescription("Description type vissage");
        // On la persiste
        $manager->persist($obj);

        // On déclenche l'enregistrement de toutes les catégories
        $manager->flush();
    }
}