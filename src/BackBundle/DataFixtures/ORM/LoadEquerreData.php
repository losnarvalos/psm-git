<?php
// src/OC/PlatformBundle/DataFixtures/ORM/LoadRoueData.php

namespace BackBundle\DataFixtures\ORM;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use BackBundle\Entity\Equerre;
use Symfony\Component\Validator\Constraints\DateTime;

class LoadEquerreData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Liste des noms de compétences à ajouter
        // On crée la compétence
        $obj = new Equerre();
        $obj->setNom("Equerre test");
        $obj->setReference("0738");
        $obj->setEmplacement("0738");
        $obj->setLongueurMin("1");
        $obj->setLongueurMax("10");
        $obj->setDescription("Petite description");
        $obj->setAngle("17");
        $obj->setPoids("30");
        $obj->setPrix("30");
        $obj->setNbTrous("30");
        $obj->setStockAlerte("30");
        $obj->setStockMini("50");
        $obj->setNbCommandes("10");
        $obj->setDateReaproDernier(new \DateTime());
        $obj->setDateReaproProchain(new \DateTime());
        $obj->setStock('50');

        // On la persiste
        $manager->persist($obj);

        // On déclenche l'enregistrement de toutes les catégories
        $manager->flush();
    }
}