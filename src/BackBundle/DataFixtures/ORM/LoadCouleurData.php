<?php
// src/OC/PlatformBundle/DataFixtures/ORM/LoadRoueData.php

namespace BackBundle\DataFixtures\ORM;

use BackBundle\Entity\Couleur;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCouleurData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Liste des noms de compétences à ajouter
        // On crée la compétence
        $obj = new Couleur();
        $obj->setHexa("#FF0000");
        $obj->setValeur("Rouge");

        // On la persiste
        $manager->persist($obj);

        // On déclenche l'enregistrement de toutes les catégories
        $manager->flush();
    }
}