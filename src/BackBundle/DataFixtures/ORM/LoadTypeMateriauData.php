<?php
// src/OC/PlatformBundle/DataFixtures/ORM/LoadRoueData.php

namespace BackBundle\DataFixtures\ORM;
use BackBundle\Entity\Materiau;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadTypeMateriauData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Liste des noms de compétences à ajouter
        // On crée la compétence
        $obj = new Materiau();
        $obj->setLibelle("Type materiaux test");
        $obj->setDescription("Description type vissage");
        // On la persiste
        $manager->persist($obj);

        // On déclenche l'enregistrement de toutes les catégories
        $manager->flush();
    }
}