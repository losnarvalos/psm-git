<?php
// src/OC/PlatformBundle/DataFixtures/ORM/LoadRoueData.php

namespace BackBundle\DataFixtures\ORM;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use BackBundle\Entity\Roue;

class LoadRoueData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Liste des noms de compétences à ajouter
        // On crée la compétence
        $obj = new Roue();
        $obj->setNom("Roue test");
        $obj->setReference("0738");
        $obj->setEmplacement("1F");
        $obj->setDiametre("17");
        $obj->setPrix("10");
        $obj->setDescription("Description Roue");
        $obj->setHauteurPneu("17");
        $obj->setNbBoulons("5");
        $obj->setPoids("5");
        $obj->setStock('50');
        $obj->setStockAlerte('10');
        $obj->setStockMini('30');
        $obj->setNbCommandes("10");
        $obj->setDateReaproDernier(new \DateTime());
        $obj->setDateReaproProchain(new \DateTime());

        // On la persiste
        $manager->persist($obj);

        // On déclenche l'enregistrement de toutes les catégories
        $manager->flush();
    }
}