<?php
// src/OC/PlatformBundle/DataFixtures/ORM/LoadRoueData.php

namespace BackBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use BackBundle\Entity\Configuration;

class LoadConfigurationData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Liste des noms de compétences à ajouter
        // On crée la compétence
        $obj = new Configuration();
        $obj->setNom("couleur_roue");
        $obj->setValeur("#f39c12");
        // On la persiste
        $manager->persist($obj);

        $obj1 = new Configuration();
        $obj1->setNom("couleur_vis");
        $obj1->setValeur("#e74c3c");
        // On la persiste
        $manager->persist($obj1);

        $obj2 = new Configuration();
        $obj2->setNom("couleur_planche");
        $obj2->setValeur("#8e44ad");
        // On la persiste
        $manager->persist($obj2);

        $obj3 = new Configuration();
        $obj3->setNom("couleur_equerre");
        $obj3->setValeur("#95a5a6");
        // On la persiste
        $manager->persist($obj3);

        $obj4 = new Configuration();
        $obj4->setNom("couleur_divers");
        $obj4->setValeur("#1abc9c");
        // On la persiste
        $manager->persist($obj4);

        $obj5 = new Configuration();
        $obj5->setNom("nomSociete");
        $obj5->setValeur("Don Pedro");
        // On la persiste
        $manager->persist($obj5);

        $obj6 = new Configuration();
        $obj6->setNom("adresseSociete");
        $obj6->setValeur("24 Rue des Portos");
        // On la persiste
        $manager->persist($obj6);

        $obj7 = new Configuration();
        $obj7->setNom("complementAdresseSociete");
        $obj7->setValeur("Dans le placard sous l'escalier");
        // On la persiste
        $manager->persist($obj7);

        $obj8 = new Configuration();
        $obj8->setNom("cpSociete");
        $obj8->setValeur("38000");
        // On la persiste
        $manager->persist($obj8);

        $obj9 = new Configuration();
        $obj9->setNom("villeSociete");
        $obj9->setValeur("Grenoble");
        // On la persiste
        $manager->persist($obj9);

        $obj10 = new Configuration();
        $obj10->setNom("SIRETSociete");
        $obj10->setValeur("351108132135435");
        // On la persiste
        $manager->persist($obj10);

        $obj11 = new Configuration();
        $obj11->setNom("nomFournisseur");
        $obj11->setValeur("Denethor II, Intendant du Gondor");
        // On la persiste
        $manager->persist($obj11);

        $obj12 = new Configuration();
        $obj12->setNom("adresseFournisseur");
        $obj12->setValeur("33 Place de l'arbre sacré");
        // On la persiste
        $manager->persist($obj12);

        $obj13 = new Configuration();
        $obj13->setNom("complementAdresseFournisseur");
        $obj13->setValeur("la Citadelle");
        // On la persiste
        $manager->persist($obj13);

        $obj14 = new Configuration();
        $obj14->setNom("cpFournisseur");
        $obj14->setValeur("84120");
        // On la persiste
        $manager->persist($obj14);

        $obj15 = new Configuration();
        $obj15->setNom("villeFournisseur");
        $obj15->setValeur("Minas Tirith");
        // On la persiste
        $manager->persist($obj15);

        $obj16 = new Configuration();
        $obj16->setNom("SIRETFournisseur");
        $obj16->setValeur("2164612329+3");
        // On la persiste
        $manager->persist($obj16);

        // On déclenche l'enregistrement de toutes les catégories
        $manager->flush();
    }
}