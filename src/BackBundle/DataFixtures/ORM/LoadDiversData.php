<?php
// src/OC/PlatformBundle/DataFixtures/ORM/LoadRoueData.php

namespace BackBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use BackBundle\Entity\Divers;

class LoadDiversData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Liste des noms de compétences à ajouter
        // On crée la compétence
        $obj = new Divers();
        $obj->setNom("Divers test");
        $obj->setReference("0738");
        $obj->setEmplacement("1F");
        $obj->setDescription("Petite description");
        $obj->setTaille("17");
        $obj->setPrix("30");
        $obj->setDateReaproDernier(new \DateTime());
        $obj->setDateReaproProchain(new \DateTime());
        $obj->setStockAlerte('10');
        $obj->setStockMini('50');
        $obj->setPoids('50');
        $obj->setNbCommandes('50');
        $obj->setStock('50');

        // On la persiste
        $manager->persist($obj);

        // On déclenche l'enregistrement de toutes les catégories
        $manager->flush();
    }
}